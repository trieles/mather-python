from tkinter import *   # Bibliothèque grapĥique Tkinter.
from cmath import *     # Fonctions pour gérer les nombres complexes.

WIDTH = 800     # Variable désignant la largeur du canevas.
HEIGHT = 600    # Variable désignant la hauteur du canevas.

coordonnees_sommets = [100, 200, 150, 250, 300, 250, 350, 200, 210, 200,
                       210, 180, 300, 180, 200, 10, 200, 200, 100, 200]

# Deux nombres complexes dont la forme algébrique est représentée par
# une liste Python : la valeur d'indice 0 est la partie réelle, celle
# d'indice 1 est la partie imaginaire.
zT = [0, 0]     # Complexe pour les translations, initialement 0
zR = [1, 0]     # Complexe pour les rotations, initialement 1

def vers_la_gauche(evenement):
    zT[0] = zT[0] - 10  # On diminue la partie réelle de 10 pixels
    print("Complexe pour les translations : zT =", zT)

def vers_la_droite(evenement):
    zT[0] = zT[0] + 10  # On augmente la partie réelle de 10 pixels
    print("Complexe pour les translations : zT =", zT)

def vers_le_haut(evenement):
    # Ne pas oublier que l'axe des ordonnées est orienté VERS LE BAS !
    zT[1] = zT[1] - 10  # On diminue la partie imaginaire de 10 pixels
    print("Complexe pour les translations : zT =", zT)

def vers_le_bas(evenement):
    zT[1] = zT[1] + 10  # On augmente la partie imaginaire de 10 pixels
    print("Complexe pour les translations : zT =", zT)

def augmenter_taille(evenement):
    print("Augmentation de la taille")

def diminuer_taille(evenement):
    print("Diminution de la taille")

def rotation_a_gauche(evenement):
    print("Rotation à gauche")

def rotation_a_droite(evenement):
    print("Rotation à droite")

f = Tk()                # Création d'une fenêtre.
f.title("Animations")   # On lui donne un titre.
# Création puis affichage du canevas.
c = Canvas(f, bg="light yellow", width=WIDTH, height=HEIGHT)
c.pack()

# Création de la figure « de référence » puis de celle qu'on déplacera.
bateau_reference = c.create_polygon(coordonnees_sommets)
bateau = c.create_polygon(coordonnees_sommets, fill="light blue",
                          outline="blue", width=5)

# On associe la fonction vers_la_gauche() à l'appui sur la touche [→].
f.bind("<Left>", vers_la_gauche)    # Notez l'absence de parenthèses ici !
f.bind("<Right>", vers_la_droite)   # Idem pour...
f.bind("<Up>", vers_le_haut)        # ...les autres fonctions...
f.bind("<Down>", vers_le_bas)       # ...et les autres touches.
f.bind("+", augmenter_taille)       # La touche + au-dessus des lettres
f.bind("<KP_Add>", augmenter_taille)        # Le + du pavé numérique
f.bind("-", diminuer_taille)        # La touche - au-dessus des lettres
f.bind("<KP_Subtract>", diminuer_taille)    # Le - du pavé numérique
f.bind("<Q>", rotation_a_gauche)
f.bind("<q>", rotation_a_gauche)    # Q et q sont différents !
f.bind("<D>", rotation_a_droite)
f.bind("<d>", rotation_a_droite)    # D et d aussi !

# On démarre la boucle principale de la fenêtre
f.mainloop()
