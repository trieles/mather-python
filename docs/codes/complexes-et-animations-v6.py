from tkinter import *
from cmath import *

WIDTH = 800
HEIGHT = 600

coordonnees_sommets = [100, 200, 150, 250, 300, 250, 350, 200, 210, 200,
                       210, 180, 300, 180, 200, 10, 200, 200, 100, 200]

def vers_la_gauche(evenement):
    zT = complex(-10, 0)
    zR = complex(1, 0)
    modifier_coordonnees(zT, zR)

def vers_la_droite(evenement):
    zT = complex(10, 0)
    zR = complex(1, 0)
    modifier_coordonnees(zT, zR)

def vers_le_haut(evenement):
    zT = complex(0, -10)
    zR = complex(1, 0)
    modifier_coordonnees(zT, zR)

def vers_le_bas(evenement):
    zT = complex(0, 10)
    zR = complex(1, 0)
    modifier_coordonnees(zT, zR)

def augmenter_taille(evenement):
    zT = complex(0, 0)
    zR = rect(1.1, 0)
    modifier_coordonnees(zT, zR)

def diminuer_taille(evenement):
    zT = complex(0, 0)
    zR = rect(0.9, 0)
    modifier_coordonnees(zT, zR)

def rotation_a_gauche(evenement):
    zT = complex(0, 0)
    zR = rect(1, 0.1)
    modifier_coordonnees(zT, zR)

def rotation_a_droite(evenement):
    zT = complex(0, 0)
    zR = rect(1, -0.1)
    modifier_coordonnees(zT, zR)

def modifier_coordonnees(zT, zR):
    for position in range(0, len(coordonnees_sommets), 2):
        z = complex(coordonnees_sommets[position],
                    coordonnees_sommets[position + 1])
        z = zR * (z + zT)
        coordonnees_sommets[position] = z.real
        coordonnees_sommets[position + 1] = z.imag
        c.coords(bateau, coordonnees_sommets)
        # c.coords(bateau, *coordonnees_sommets)    # Pour un vieux Python


f = Tk()
f.title("Animations")
c = Canvas(f, bg="light yellow", width=WIDTH, height=HEIGHT)
c.pack()
bateau_reference = c.create_polygon(coordonnees_sommets)
bateau = c.create_polygon(coordonnees_sommets, fill="light blue",
                          outline="blue", width=5)

f.bind("<Left>", vers_la_gauche)
f.bind("<Right>", vers_la_droite)
f.bind("<Up>", vers_le_haut)
f.bind("<Down>", vers_le_bas)
f.bind("+", augmenter_taille)
f.bind("<KP_Add>", augmenter_taille)
f.bind("-", diminuer_taille)
f.bind("<KP_Subtract>", diminuer_taille)
f.bind("<Q>", rotation_a_gauche)
f.bind("<q>", rotation_a_gauche)
f.bind("<D>", rotation_a_droite)
f.bind("<d>", rotation_a_droite)

f.mainloop()
