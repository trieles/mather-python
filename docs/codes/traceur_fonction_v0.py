from tkinter import *

def f(x):
    return 11/3000*(x-100)**2+50

cs = []
for x in range(-100, 700, 50):
    cs.append(x)
    cs.append(f(x))

f = Tk()
c = Canvas(f, width=600, height=400, background="light yellow")
c.pack()

C_f = c.create_line(cs, fill="red", width=5)
c.update()
c.postscript(file="fonction_nok.ps", colormode='color')

f.mainloop()
