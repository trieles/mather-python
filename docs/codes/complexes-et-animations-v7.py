# Cf. https://bytes.com/topic/python/answers/31782-canvas-polygon-coords-using-list-comprehension
# import tkinter        # Utile pour accéder à la commande tkinter._flatten
from tkinter import *
from cmath import *

WIDTH = 800
HEIGHT = 600

coordonnees_sommets = [100, 200, 150, 250, 300, 250, 350, 200, 210, 200,
                       210, 180, 300, 180, 200, 10, 200, 200, 100, 200]

coordonnees_centre_rotation = [225, 130]

def vers_la_gauche(evenement):
    modifier_coordonnees(zT=complex(-10, 0))

def vers_la_droite(evenement):
    modifier_coordonnees(zT=complex(10, 0))

def vers_le_haut(evenement):
    modifier_coordonnees(zT=complex(0, -10))

def vers_le_bas(evenement):
    modifier_coordonnees(zT=complex(0, 10))

def augmenter_taille(evenement):
    modifier_coordonnees(zR=rect(1.1, 0))

def diminuer_taille(evenement):
    modifier_coordonnees(zR=rect(0.9, 0))

def rotation_a_gauche(evenement):
    modifier_coordonnees(zR=rect(1, 0.1))

def rotation_a_droite(evenement):
    modifier_coordonnees(zR=rect(1, -0.1))

def modifier_coordonnees(zT=complex(0, 0), zR=complex(1, 0)):
    for position in range(0, len(coordonnees_sommets), 2):
        z = complex(coordonnees_sommets[position],
                    coordonnees_sommets[position + 1])
        zC = complex(coordonnees_centre_rotation[0], coordonnees_centre_rotation[1])
        z = zR * (z + zT - zC) + zC
        coordonnees_sommets[position] = z.real
        coordonnees_sommets[position + 1] = z.imag
        c.coords(bateau, coordonnees_sommets)
        # c.coords(bateau, *coordonnees_sommets)    # Pour un vieux Python

def deplacer_centre(dx, dy):
    coordonnees_centre_rotation[0] += dx
    coordonnees_centre_rotation[1] += dy
    c.move(centre_rotation, dx, dy)


f = Tk()
f.title("Animations")
c = Canvas(f, bg="light yellow", width=WIDTH, height=HEIGHT)
c.pack()
bateau_reference = c.create_polygon(coordonnees_sommets)
bateau = c.create_polygon(coordonnees_sommets, fill="light blue",
                          outline="blue", width=5)
centre_rotation = c.create_oval(coordonnees_centre_rotation[0]-3,
                                coordonnees_centre_rotation[1]-3, 
                                coordonnees_centre_rotation[0]+3, 
                                coordonnees_centre_rotation[1]+3, fill="red")

f.bind("<Left>", vers_la_gauche)
f.bind("<Right>", vers_la_droite)
f.bind("<Up>", vers_le_haut)
f.bind("<Down>", vers_le_bas)
f.bind("+", augmenter_taille)
f.bind("<KP_Add>", augmenter_taille)
f.bind("-", diminuer_taille)
f.bind("<KP_Subtract>", diminuer_taille)
f.bind("<Q>", rotation_a_gauche)
f.bind("<q>", rotation_a_gauche)
f.bind("<D>", rotation_a_droite)
f.bind("<d>", rotation_a_droite)
f.bind("<i>", lambda x: deplacer_centre(0, -10))
f.bind("<I>", lambda x: deplacer_centre(0, -10))
f.bind("<k>", lambda x: deplacer_centre(0, 10))
f.bind("<K>", lambda x: deplacer_centre(0, 10))
f.bind("<j>", lambda x: deplacer_centre(-10, 0))
f.bind("<J>", lambda x: deplacer_centre(-10, 0))
f.bind("<l>", lambda x: deplacer_centre(10, 0))
f.bind("<L>", lambda x: deplacer_centre(10, 0))

f.mainloop()
