from tkinter import *   # Tkinter
from cmath import *     # Fonctions pour gérer les nombres complexes.

WIDTH = 800     # Variable désignant la largeur du canevas
HEIGHT = 600    # Variable désignant la hauteur du canevas

coordonnees_sommets = [100, 200, 150, 250, 300, 250, 350, 200, 210, 200,
                       210, 180, 300, 180, 200, 10, 200, 200, 100, 200]

f = Tk()    # Création d'une fenêtre
c = Canvas(f, bg="light yellow", width=WIDTH, height=HEIGHT)    # Création du canevas
c.pack()    # Affichage du canevas

bateau_reference = c.create_polygon(coordonnees_sommets)
bateau = c.create_polygon(coordonnees_sommets, fill="light blue", outline="blue", width=5)

f.mainloop()
