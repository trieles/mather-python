from tkinter import *

N = 100         # Nombre de points pour le tracé de la courbe.
L = 800         # Largeur du canevas.
H = 800         # Hauteur du canevas.

x_min = -5      # La fenêtre d'affichage mathématique
x_max = 10      # pour le tracé de la courbe
y_min = -15     # de la fonction f
y_max = 35      # définie plus bas.

# Coefficients de proportionnalité entre les dimensions
# du canevas informatique et celles de la fenêtre mathématique.
x_ratio = L / (x_max - x_min)   # En abscisse...
y_ratio = H / (y_max - y_min)   # ...et en ordonnée.

# Abscisse de l'origine O dans le repère informatique :
x_O_aff = abs(x_min) * x_ratio
  # Ordonnée de l'origine O dans le repère informatique :
y_O_aff = abs(y_max) * y_ratio

# La fonction f dont on souhaite tracer la courbe :
def f(x):
    return -x**2+35

# Conversion d'une ABSCISSE MATHÉMATIQUE en ABSCISSE dans le CANEVAS.
def x_to_xaff(x):
    return x * x_ratio + x_O_aff

# Conversion d'une ORDONNÉE MATHÉMATIQUE en ORDONNÉE dans le CANEVAS.
def y_to_yaff(y):
    return -1 * y * y_ratio + y_O_aff

# Les coordonnées des points qui seront utilisés pour créer la courbe.
cs = []
for i in range(N+1):    # >>> COMPLÉTER LES POINTILLÉS !
    x = x_min + i * (x_max - x_min) / N
    cs.append(x_to_xaff(x))
    cs.append(y_to_yaff(f(x)))

# Le code pour l'interface graphique.
f = Tk()
c = Canvas(f, width=L, height=H, background="light yellow")
c.pack()
C_f = c.create_line(cs, fill="red", width=5)
# Pour enregistrer une image (vectorielle) du résultat :
c.update()
c.postscript(file="fonction_ok.ps", colormode='color')
# Boucle principale de la fenêtre.
f.mainloop()
