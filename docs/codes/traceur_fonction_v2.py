from tkinter import *

N = 100         # Nombre de points pour le tracé de la courbe.
L = 800         # Largeur du canevas.
H = 800         # Hauteur du canevas.

x_min = -5      # La fenêtre d'affichage mathématique
x_max = 5      # pour le tracé de la courbe
y_min = -1     # de la fonction f
y_max = 26      # définie plus bas.

# Coefficients de proportionnalité entre les dimensions
# du canevas informatique et celles de la fenêtre mathématique.
x_ratio = L / (x_max - x_min)   # En abscisse...
y_ratio = H / (y_max - y_min)   # ...et en ordonnée.

# Abscisse de l'origine O dans le repère informatique :
x_O_aff = abs(x_min) * x_ratio
  # Ordonnée de l'origine O dans le repère informatique :
y_O_aff = abs(y_max) * y_ratio

def f(x):
    return x**2

def x_to_xaff(x):
    return x * x_ratio + x_O_aff

def y_to_yaff(y):
    return -1 * y * y_ratio + y_O_aff

# ~ xs = [ x for x in range(x_min, x_max + 1, 1) ]  # Ne convient pas !
# En effet, on n'a pas créé ici N abscisses différentes !
# ~ xs = [ x_min + i * (x_max - x_min) / N for i in range(N+1) ]
# ~ fxs = [ f(x) for x in xs ]
# ~ xs_aff = [ x_to_xaff(x) for x in xs ]
# ~ fxs_aff = [ y_to_yaff(fx) for fx in fxs ]
# Variante plus courte (mais moins lisible)
cs = [ g(x) for x in [ x_min + i * (x_max - x_min) / N for i in range(N+1) ] for g in (x_to_xaff, lambda t : y_to_yaff(f(t))) ]

# ~ for i in range(N+1):
    # ~ x = x_min + i * (x_max - x_min) / N
    # ~ cs.append(x_to_xaff(x))
    # ~ cs.append(y_to_yaff(f(x)))

f = Tk()
c = Canvas(f, width=L, height=H, background="light yellow")
c.pack()

Ox = c.create_line(0, y_O_aff, L, y_O_aff, fill="blue", width=3, arrow="last")
for x in range(x_min, x_max+1):
    x_aff = x_to_xaff(x)
    c.create_line(x_aff, .99*y_O_aff, x_aff, 1.01*y_O_aff, fill="blue", width=3)
    c.create_text(x_aff, 1.03*y_O_aff, text=str(x), fill="blue")
Oy = c.create_line(x_O_aff, H, x_O_aff, 0, fill="blue", width=3, arrow="last")
for y in range(y_min, y_max+1):
    y_aff = y_to_yaff(y)
    c.create_line(.99*x_O_aff, y_aff, 1.01*x_O_aff, y_aff, fill="blue", width=3)
    c.create_text(.96*x_O_aff, y_aff, text=str(y), fill="blue")

C_f = c.create_line(cs, fill="red", width=5)

c.update()
c.postscript(file="fonction_axes.ps", colormode='color')

f.mainloop()
