from tkinter import *   # Bibliothèque grapĥique Tkinter.
from cmath import *     # Fonctions pour gérer les nombres complexes.

WIDTH = 800     # Variable désignant la largeur du canevas.
HEIGHT = 600    # Variable désignant la hauteur du canevas.

coordonnees_sommets = [100, 200, 150, 250, 300, 250, 350, 200, 210, 200,
                       210, 180, 300, 180, 200, 10, 200, 200, 100, 200]

def vers_la_gauche(evenement):
    zT = complex(-10, 0)    # On diminue la partie réelle de 10 pixels
    zR = complex(1, 0)      # Aucune rotation-dilatation
    modifier_coordonnees(zT, zR)

def vers_la_droite(evenement):
    zT = complex(10, 0)     # On augmente la partie réelle de 10 pixels
    zR = complex(1, 0)      # Aucune rotation-dilatation
    modifier_coordonnees(zT, zR)

def vers_le_haut(evenement):
    # Ne pas oublier que l'axe des ordonnées est orienté VERS LE BAS !
    zT = complex(0, -10)    # On diminue la partie imaginaire de 10 pixels
    zR = complex(1, 0)      # Aucune rotation-dilatation
    modifier_coordonnees(zT, zR)

def vers_le_bas(evenement):
    zT = complex(0, 10)     # On augmente la partie imaginaire de 10 pixels
    zR = complex(1, 0)      # Aucune rotation-dilatation
    modifier_coordonnees(zT, zR)

def augmenter_taille(evenement):
    zT = complex(0, 0)      # Aucune translation
    zR = rect(1.1, 0)       # On augmente le module de 10%
    modifier_coordonnees(zT, zR)

def diminuer_taille(evenement):
    zT = complex(0, 0)      # Aucune translation
    zR = rect(0.9, 0)       # On diminue le module de 10%
    modifier_coordonnees(zT, zR)

def rotation_a_gauche(evenement):
    zT = complex(0, 0)      # Aucune translation
    zR = rect(1, 0.1)       # On augmente l'argument de 0.1 radian
    modifier_coordonnees(zT, zR)

def rotation_a_droite(evenement):
    zT = complex(0, 0)      # Aucune translation
    zR = rect(1, -0.1)      # On diminue l'argument de 0.1 radian
    modifier_coordonnees(zT, zR)

def modifier_coordonnees(zT, zR):
    for position in range(0, len(coordonnees_sommets), 2):
        # Chaque valeur de la variable « position » donnera accès dans la
        # liste « coordonnees_sommets » à une abscisse ; de même, on y
        # trouvera une ordonnée à chaque valeur de « position_suivante »
        position_suivante = position + 1
        partie_reelle = coordonnees_sommets[position]
        partie_imaginaire = coordonnees_sommets[position_suivante]
        z = complex(partie_reelle, partie_imaginaire)
        z = z + zT          # On effectue la translation associée au complexe zT
        z = z * zR          # On effectue la rotation associée au complexe zR
        nouvelle_partie_reelle = z.real     # La nouvelle abscisse du sommet
        nouvelle_partie_imaginaire = z.imag # La nouvelle ordonnée du sommet
        # On mémorise les nouvelles coordonnées dans la liste qui définit la figure
        coordonnees_sommets[position] = nouvelle_partie_reelle
        coordonnees_sommets[position_suivante] = nouvelle_partie_imaginaire
        # On demande au canevas de Tkinter de prendre en compte les nouvelles
        # coordonnées des sommets de la figure
        c.coords(bateau, coordonnees_sommets)   # ATTENTION ! Pour d'anciennes
        # versions de Python (par exemple la 3.4), il faudra écrire :
        # c.coords(bateau, *coordonnees_sommets)    # Notez l'étoile ici !


f = Tk()                # Création d'une fenêtre.
f.title("Animations")   # On lui donne un titre.
# Création puis affichage du canevas.
c = Canvas(f, bg="light yellow", width=WIDTH, height=HEIGHT)
c.pack()

# Création de la figure « de référence » puis de celle qu'on déplacera.
bateau_reference = c.create_polygon(coordonnees_sommets)
bateau = c.create_polygon(coordonnees_sommets, fill="light blue",
                          outline="blue", width=5)

# On associe la fonction vers_la_gauche() à l'appui sur la touche [→].
f.bind("<Left>", vers_la_gauche)    # Notez l'absence de parenthèses ici !
f.bind("<Right>", vers_la_droite)   # Idem pour...
f.bind("<Up>", vers_le_haut)        # ...les autres fonctions...
f.bind("<Down>", vers_le_bas)       # ...et les autres touches.
f.bind("+", augmenter_taille)       # La touche + au-dessus des lettres
f.bind("<KP_Add>", augmenter_taille)        # Le + du pavé numérique
f.bind("-", diminuer_taille)        # La touche - au-dessus des lettres
f.bind("<KP_Subtract>", diminuer_taille)    # Le - du pavé numérique
f.bind("<Q>", rotation_a_gauche)
f.bind("<q>", rotation_a_gauche)    # Q et q sont différents !
f.bind("<D>", rotation_a_droite)
f.bind("<d>", rotation_a_droite)    # D et d aussi !

# On démarre la boucle principale de la fenêtre
f.mainloop()
