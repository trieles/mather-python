# Nombres complexes et animations graphiques avec Python

??? warning "Avertissement important"

    Vous **devez** avoir au préalable suivi le document sur le [tracé de fonctions
    avec Python et Tkinter](../20-tracer-fonction){target='__blank' rel='noopener'}
    ainsi que l'[introduction aux nombres complexes](../50-complexes){target='__blank' rel='noopener'}.
    Si, en plus, vous avez également lu le document d'[introduction à Tkinter pour la NSI](https://trieles.gitlab.io/nsi/20-tkinter){target='__blank' rel='noopener'},
    vous n'en serez que plus à l'aise !

## Le programme de base

On part du code ci-dessous, qui se contente  de créer une fenêtre, qui comporte
un canevas de 800 pixels sur 600 pixels, dans lequel on a définit une figure.

**Vous recopierez ce code dans un script Python nommé `complexes-et-animations.py`**
que vous compléterez au fur et à mesure.

```python
--8<-- "docs/codes/complexes-et-animations-v0.py"
```

## Associer un évènement à une touche

On veut déplacer notre figure au clavier : on va donc naturellement chercher
à faire en sorte que notre programme réagisse lorsqu'on appuie sur l'une
des quatre touches de direction : ++"←"++, ++"→"++, ++"↑"++ et ++"↓"++.
**On dit qu'on va associer un évènement à une fonction**.

Plus précisément, on va définir quatre fonctions, qui au début ne feront
pas grand chose à part afficher un bref message en console, et les associer
à chacune des touches directionnelles du clavier.

**Observez, saisissez puis exécutez le code ci-dessous :** lorsque vous
appuyez sur les flèches du clavier, un message s'affiche en console _(vous
devrez peut-être déplacer la fenêtre de votre application pour pouvoir observer
la console ! et si c'est le cas, pensez à re-cliquer dans la fenêtre affichant
la figure de sorte à ce que celle-ci ait le « focus », c'est-à-dire que
ce soit bien **elle** qui reçoive les évènements associés aux appuis sur les
touches)_.

=== "Code"

    ```python
    --8<-- "docs/codes/complexes-et-animations-v1.py"
    ```

=== "Rendu"

    <center>
    ![](images/association-touche-fonction.gif)
    </center>


??? info "Remarques"

    **Associer une touche à une action**
    : Cela se fait au moyen de l'instruction `#!python f.bind(...)`, où
    `f` désigne ici la fenêtre principale de l'application qu'on développe.
    On passe deux paramètres à la méthode `bind()` :
    * en premier, une chaîne de caractères représentant le nom d'un touche
    du clavier (ou un bouton de la souris), suivi du **nom** de la fonction
    qu'on veut associer.
    
        !!! warning "Juste le nom, SANS les parenthèses !!!"
            
            Mettre les parenthèse signifie que vous **exigez** de Python
            qu'il **exécute** la fonction ! Or on ne souhaite son exécution
            **que** lorsque survient l'appui sur la touche (ou sur le bouton
            de la souris) !
    
    **Rôle du paramètre `evenement`**
    : Lorsqu'on utilise l'instruction `bind` pour _lier_ un composant graphique
    (appelé _widget_ en anglais) à une fonction, Tkinter fabrique _automatiquement_
    un objet informatique comportant des informations diverses, qu'il passe
    en paramètre à la fonction liée. Parmi ces informations, on peut trouver
    (en fonction de la nature de l'évènement associé, pression d'une touche
    ou d'un bouton de souris) le nom de la touche, le numéro du bouton,
    les coordonnées du point sur lequel le clic a eu lieu, etc.

!!! note "À faire vous-même"

    === "Votre objectif"

        Créez 4 associations supplémentaires :
        
        * une fonction `augmenter_taille(...)` avec la touche ++plus++ ;
        * une fonction `diminuer_taille(...)` avec la touche ++minus++ ;
        * une fonction `rotation_a_gauche(...)` avec la touche ++q++ ;
        * une fonction `rotation_a_droite(...)` avec la touche ++d++.

    === "Indications"

        * Consultez la [documentation de Tkinter](http://tkinter.fdex.eu/doc/event.html#noms-des-touches){target='__blank' rel='noopener'}
        pour trouver le nom des touches adéquates.
        
        * Petite subtilité : pensez à la casse et au fait qu'un ordinateur
        portable n'a pas nécessairement de pavé numérique !

    === "Solution"

        Il faut définir les 4 fonctions :
        
        ```python
        def augmenter_taille(evenement):
            print("Augmentation de la taille")

        def diminuer_taille(evenement):
            print("Diminution de la taille")

        def rotation_a_gauche(evenement):
            print("Rotation à gauche")

        def rotation_a_droite(evenement):
            print("Rotation à droite")
        ```
        
        et... pas moins de 8 associations, afin de gérer proprement toutes les
        situations possibles !
        ```python
        f.bind("+", augmenter_taille)       # La touche + au-dessus des lettres
        f.bind("<KP_Add>", augmenter_taille)        # Le + du pavé numérique
        f.bind("-", diminuer_taille)        # La touche - au-dessus des lettres
        f.bind("<KP_Subtract>", diminuer_taille)    # Le - du pavé numérique
        f.bind("<Q>", rotation_a_gauche)
        f.bind("<q>", rotation_a_gauche)    # Q et q sont différents !
        f.bind("<D>", rotation_a_droite)
        f.bind("<d>", rotation_a_droite)    # D et d aussi !
        ```


## Avoir des associations _utiles_

??? info "Rappels sur la relation entre nombres complexes et transformations géométriques"

    D'après l'[introduction aux nombres complexes](../50-complexes){target='__blank' rel='noopener'},
    vous savez qu'ajouter un même nombre complexe à chacun des complexes qui
    correspondent aux sommets de notre figure revient à effectuer une translation
    de chacun de ces sommets, et donc de la figure dans son ensemble.
    
    De même, si l'on multiplie par un même nombre complexe (mais différent
    du précédent) chacune des affixes correspondant aux sommets de la figure,
    on réussira à lui faire subir une rotation-dilatation.

On sait désormais associer une action à une touche. Il faut maintenant
qu'à travers elles soient définis **deux** nombres complexes :

* **`zT` gouvernera les translations.** C'est ce nombre complexe qui
sera _ajouté_ à l'affixe de chaque sommet de la figure, afin de la déplacer
(sans modification) verticalement et horizontalement ;

* **`zR` présidera aux rotations-dilatations.** C'est ce nombre complexe
qui viendra _multiplier_ l'affixe de chaque sommet de la figure, afin de
l'agrandir ou de la rétrécir, ou de la faire tourner dans un sens ou dans l'autre.

Nous devons maintenant choisir l'_ampleur_ des modifications que ces nombres
complexes apporteront, ce qui signifie que nous devons décider des valeurs
de ces nombres complexes.

* On décidera que les translations feront toujours 10 pixels, peu importe
le sens ou la direction.

    <center>

    | Appui sur la touche :                  | ++"←"++        | ++"→"++        | ++"↑"++       | ++"↓"++       |
    |---------------------------------------:|:---------------:|:--------------:|:--------------:|:--------------:|
    | À l'affixe de chaque sommet on ajoute :|$-10+0\mathbf i$ |$10+0\mathbf i$ |$0-10\mathbf i$ |$0+10\mathbf i$ |
    | <small>_Traduction en terme de coordonnées :_</small> | <small>_L'abscisse de chaque sommet diminue de 10 pixels_</small> | <small>_L'abscisse de chaque sommet augmente de 10 pixels_</small> | <small>_L'ordonnée de chaque sommet **diminue** de 10 pixels_</small> | <small>_L'ordonnée de chaque sommet **augmente** de 10 pixels_</small> |

    </center>

    ??? warning "ATTENTION : l'axe des ordonnées est ici dirigé _VERS LE BAS_ !"

        C'est très souvent le cas, en informatique, ça n'est pas uniquement
        valable pour le langage Python. Pour plus de détails, vous pouvez vous
        reporter à la [« Note historique » de ce document](https://trieles.gitlab.io/nsi/2-tkinter#le-widget-canevas){target='__blank' rel='noopener'}.


* On décidera que les dilatations multiplieront par $1,\!1$ le module de l'affixe
de chaque sommet,** quand les contractions les multiplieront par $0,\!9$. Les
rotations _dans le sens trigonométrique_ ajouteront $0,\!1$ radian au module
de l'affixe de chaque sommet, alors que les rotations dans le sens contraire
(le sens horaire, donc) retrancheront $0,\!1$ radian à ces modules.

    <center id="rotation-dilatation">

    | Appui sur la touche :                         | ++plus++                 | ++minus++                | ++q++                    | ++d++                     |
    |----------------------------------------------:|:------------------------:|:------------------------:|:------------------------:|:-------------------------:|
    | L'affixe de chaque sommet est multipliée par :| $\big[ 1,\!1 \, ; 0 \big]$ | $\big[ 0,\!9 \, ; 0 \big]$ | $\big[ 1 \, ; 0,\!1 \big]$ | $\big[ 1 \, ; -0,\!1 \big]$ |

    </center>

    ??? question "Êtes-vous au point sur les pourcentages ?"
            
        === "Question"

            Les valeurs choisies pour faire évoluer les modules des affixes
            des sommets de la figure pourraient vous faire réagir, si vous avez
            acquis de bons réflexes...
            
        === "Indication"
        
            Traduisez en pourcentage l'évolution associée au coefficient
            multiplicateur $1,\!1$ puis faites de même avec $0,\!9$. Ces
            deux évolutions sont-elles vraiment _réciproques_ l'une de
            l'_autre_ ?
        
        === "Solution"
        
            Le coefficient multiplicateur $1,\!1$ correspond à une augmentation
            de 10 %. Le coefficient multiplicateur $0,\!9$ correspond à
            une diminution de 10 %. Le coefficient multiplicateur associé
            à l'enchaînement de ces deux évolutions est $1,\!1 \times 0,\!9 = 0,\!99 \neq 1$,
            qui correspond à une diminution de 1 % : ces deux évolutions
            ne se compensent donc pas exactement !
            
            Ainsi, avec ces choix de valeurs, une fois la figure dilatée
            ou rétrécie une première fois, il ne sera **plus jamais** possible
            de lui faire retrouver sa taille initiale...

??? warning "Ces deux objectifs ne présentent pas tout à fait le même niveau de difficulté !"

    Gérer les translations est un objectif assez facile à atteindre : on n'a
    qu'à agir sur les parties réelles et imaginaires de chaque affixe d'un
    sommet de la figure. Concrètement,   
    Par contre, gérer les rotations-dilatations sera un peu plus délicat,
    car il nous faudra agir sur le module ou l'argument du 2^nd^ nombre
    complexe... mais comme vous allez le voir, Python va nous simplifier
    la tâche :wink: !

Reste une question essentielle !

<center>
**:interrobang::thinking: Python peut-il gérer les nombres complexes ? Et si oui, comment ? :thinking::interrobang:**
</center>

Facile ! **Python dispose par défaut d'un _objet_ « nombre complexe »** et il nous
faut d'abord nous familiariser avec, et notamment apprendre comment passer
de la forme algébrique à la forme géométrique — et réciproquement.

**Dans Idle, en mode « commande », saisissez les instructions suivantes :**

```pycon
>>> from cmath import * # Augmente les capacités de Python sur les complexes
>>> z = complex(1, 1)   # Crée un nombre complexe affecté à la variable z
>>> z.real              # La partie réelle du nombre complexe z
>>> z.imag              # La partie imaginaire du nombre complexe z
>>> rho = abs(z)        # Calcule le module du nombre complexe z
>>> theta = phase(z)    # Calcule l'argument du nombre complexe z
>>> rect(rho, theta)    # Conversion forme géométrique -> forme algébrique
```

??? warning "Remarque délicate : calculer avec les nombres décimaux est impossible en informatique !"

    === "Pourquoi ?"

        Certaines fractions n'ont pas d'écriture décimale _finie_ car, lorsqu'on
        pose la division, elle ne s'arrête jamais. C'est ainsi le cas avec $\dfrac13 = 0,\!3333\ldots$.
        Écrire un nombre en base 10 revient à le décomposer selon les puissances
        de 10 positives successives pour sa partie entière, puis selon les
        puissances _négatives_ de 10 pour sa partie décimale. Ainsi,
        $5\,203,\!15 = 5 \times 10^{3} + 2 \times 10^2 + 0 \times 10^1 + 3 \times 10^0 + 1 \times 10^{-1} + 5 \times 10^{-2}$.
        
        De même, écrire un nombre en base 2 (en _binaire_, donc) revient
        à le décomposer selon les puissances de 2 positives successives
        pour sa partie entière, puis selon les puissances _négatives_ de
        2 pour sa partie décimale. Ainsi,
        $0,\!625 = 0 \times 2^{0} + 1 \times 2^{-1} + 0 \times 10^{-2} + 1 \times 2^{-3}$.
        On peut donc écrire $0,\!625_d = 0,0101_b$ (le $_d$ signalant une
        écriture en base 10, le  $_b$ indiquant une écriture en base 2).
        
        La décomposition de certains nombres ne s'effectue pas bien en base
        10, et il en va de même en base 2 : $0,\!1$ est l'un de ses nombres !
        On peut montrer que $0,\!1_d = 0,\!000\,1100\,1100\,1100\ldots{}_b$.
        
        **Ainsi, un ordinateur ne peut pas calculer précisément avec _tous_
        les nombres décimaux !**
        
        En raison des erreurs d'arrondis induits par cette difficulté qu'il
        n'est pas possible d'éviter de manière absolue, on ne retrouve pas
        exactement le nombre complexe initial. En effet, `z` désignait le
        nombre complexe `#!python (1+1j)` (soit $1+\mathbf i$ en notation
        mathématique), alors que l'instruction `#!python rect(rho, theta)`
        renvoie la valeur `#!python (1.0000000000000002+1j)` !
        
        Mais on peut avoir des surprises bien plus... surprenantes encore, avec
        Python en particulier, mais aussi avec n'importe quel langage de programmation
        généraliste ! Essayez par exemple :
        
        ```pycon
        >>> a = 0
        >>> a = a + 0.1
        >>> a = a + 0.1
        >>> a = a + 0.1     # Surprise !
        ```
    
    === "Pour aller plus loin"
    
        On peut observer « mieux », si l'on peut dire : en répétant cet ajout
        de `#!python 0.1` à partir de la valeur initiale `#!python 0`, tout d'abord
        100 fois, puis 200 fois, puis 300 fois, on observe que les erreurs de
        calculs se traduisent parfois par des résultats qui excèdent la valeur
        théorique, parfois par des résultats qui lui sont inférieurs ! Vous
        seriez-vous attendu à un tel phénomène ?
        
        <center>
        ![](images/incrementations-0.1.gif)
        </center>

    === "Comment décomposer avec Python `#!python 0.1` en base 2 ?"
    
        Analysez et exécutez le code ci-dessous :
        
        ```pycon
        >>> approximation = 0
        >>> ecriture_binaire = []
        >>> for i in range(30):
                temp = approximation + 2**(-i)
                if temp > 0.1:
                    ecriture_binaire.append(0)
                else:
                    ecriture_binaire.append(1)
                    approximation = temp
                print(d)
        >>> print(ecriture_binaire)
        ```


### Définir le complexe qui gouverne les translations

Nous avons décidé qu'à **chaque appui sur une touche directionnelle, 10
pixels seront ajoutés ou retranchés** à la partie réelle ou à la partie
imaginaire (tout dépend des touches pressées, naturellement) de l'affixe
de chaque sommet composant la figure.

Ainsi, dans la fonction Python `#!python vers_la_gauche(...)`, nous devons
définir le nombre complexe $z_T = -10 + 0\mathbf i$. Cela se fait simplement
à l'aide de l'instruction `#!python zT = complex(-10, 0)` !

De même, on définira pour la fonction `#!python vers_le_haut(...)` le nombre
complexe `#!python zT = complex(0, -10)` (souvenez-vous que l'axe des ordonnées est
orienté _vers le bas_ !).

Le code de ces fonctions devient alors :

```python
def vers_la_gauche(evenement):
    zT = complex(-10,0)     # On diminue la partie réelle de 10 pixels
    print("Complexe pour les translations : zT =", zT)

def vers_le_haut(evenement):
    # Ne pas oublier que l'axe des ordonnées est orienté VERS LE BAS !
    zT = complex(0, -10)    # On diminue la partie imaginaire de 10 pixels
    print("Complexe pour les translations : zT =", zT)
```

!!! note "À faire vous-même"

    === "Votre objectif"

        Modifiez les deux autres fonctions `#!python vers_la_droite(...)` et
        `#!python vers_le_bas(...)`.
           
    === "Indication"

        N'oubliez pas que l'axe des ordonnées est orienté **vers le bas** en
        Python (c'est souvent le cas en informatique en général). Pour plus
        de détails, vous pouvez vous reporter à la [« Note historique » de ce
        document](https://trieles.gitlab.io/nsi/2-tkinter#le-widget-canevas){target='__blank' rel='noopener'}.

    === "Solution"

        _Sérieusement_ ? Vous abusez :confused: ...
        ```python
        def vers_la_droite(evenement):
            zT = complex(10, 0)     # On augmente la partie réelle de 10 pixels
            print("Complexe pour les translations : zT =", zT)

        def vers_le_bas(evenement):
            zT = complex(0, 10)     # On augmente la partie imaginaire de 10 pixels
            print("Complexe pour les translations : zT =", zT)
        ```


### Définir le complexe qui gouverne les rotations-dilatations

On doit créer le [nombre complexe adapté](#rotation-dilatation) dans les
fonctions gérant dilatation, contraction et rotations. Recopiez le code
ci-dessous dans votre script, en _remplacement_ des fonctions existantes
lorsque c'est pertinent :

```python
def augmenter_taille(evenement):
    zR = rect(1.1, 0)   # Module 1.1 (pour dilatation), argument nul (aucune rotation)
    print("Complexe pour l'augmentation de la taille : zR =", zR)

def rotation_a_gauche(evenement):
    zR = rect(1, 0.1)   # Module 1 (aucune dilatation), argument 0.1 (rotation de 0.1 rad)
    print("Complexe pour rotation à gauche : zR =", zR)
```

Exécutez alors votre script Pyhton :

* observez l'évolution de l'affichage en console lorsque vous appuyez sur
les touches ++plus++ et ++q++ (veillez à ce que la fenêtre Tkinter ait bien
le « focus » avant d'appuyer sur les touches, autrement ce sera la fenêtre
de la console qui « recevra » les pressions de touches, et n'en fera rien !) ;
* comparez le résultat avec l'affichage sur les touches ++minus++ et ++d++.

!!! note "À faire vous-même"

    === "Votre objectif"

        Modifiez les deux autres fonctions `#!python diminuer_taille(...)` et
        `#!python rotation_a_droite(...)`.
            
    === "Indication"

        Aucune n'est nécessaire ici, il n'y a aucun piège, aucune difficulté.

    === "Solution"

        _Sérieusement_ ? Vous abusez encore plus :face_with_raised_eyebrow: !
        ```python
        def diminuer_taille(evenement):
            zR = rect(0.9, 0)   # Module 0.9 (pour contraction), argument nul (aucune rotation)
            print("Complexe pour la diminution de la taille : zR =", zR)

        def rotation_a_droite(evenement):
            zR = rect(1, -0.1)  # Module 1 (aucune dilatation), argument -0.1 (rotation de -0.1 rad)
            print("Complexe pour rotation à droite : zR =", zR)
        ```


**Conclusion :** on a bien créé les nombres complexes indispensables,
mais on n'observe toujours aucune évolution de la figure. C'est
_normal_, on n'a encore pas agi sur les coordonnées des sommets qui la
composent (et donc des affixes correspondantes)...


## Modifier la figure (rappels sur Python et les listes)

C'est la dernière étape !

* Il faut parcourir la liste des coordonnées qui définissent notre figure.
* Chaque **paire** de coordonnées consécutives définit un des sommets de
la figure. À partir d'elles, on devra fabriquer un nombre complexe (rappel :
on le qualifie d'_affixe_ du sommet correspondant).
* À cette affixe, on _ajoutera_ le nombre complexe $z_T$ (`zT` en Python)
qui régit les translations.
* Puis on _multipliera_ le résultat par le nombre complexe  $z_R$ (`zR`
en Python) qui gouverne les rotations-dilatations.
* Le nouveau nombre complexe obtenu nous donnera, à travers ses parties
réelle et imaginaire, les nouvelles coordonnées du sommet concerné de la
figure.
* À l'issue de chaque étape, on devra modifier la variable `coordonnees_sommets`,
qui mémorise la suite des abscisses et ordonnées de chaque sommet.
* Enfin, on pourra exiger du canevas Python qu'il fasse évoluer l'affichage.
C'est alors que la figure sera modifiée comme on le souhaite !

<center>^:warning:^ **Il y a plusieurs difficultés !** ^:warning:^</center>

<span id="rappels-python-boucles-listes-tests"></span>

??? warning "Comment faire en Python : rappels sur les boucles, les listes et les tests"

    1. Parcourir la liste des coordonnées et les grouper par **paires**.
    Il y a essentiellement deux approches possibles :
    
        * parcourir les positions **intéressantes** de la liste, c'est-à-dire
        0, puis 2, puis 4, etc. jusqu'à l'avant-dernière position de la
        liste. On « saute » donc de 2 en 2 dans les positions de la liste.
        
            ??? done "Parcourir une liste par position avec une « boucle pour » et l'instruction `#!python range`"
                
                La syntaxe de l'instruction `#!python range(...)` offre
                3 possibilités :
                
                * `#!python for i in range(fin)` : la variable `i` prendra successivement
                les valeurs `0`, `1`, `2`, etc. jusqu'à `fin` **exclue** !
                La dernière valeur de `i` sera donc `fin - 1`.
                
                * **lorsqu'on ne connaît pas à l'avance le nombre d'éléments
                d'une liste,** on peut le déterminer à l'aide de la fonction
                `#!python len(...)` (`len` est l'abréviation de _length_ qui, en
                anglais, signifie « longueur »). Pour _afficher_ les éléments
                d'une `liste` en console, on peut donc utiliser le code :
                ```python
                for in in range(len(liste)):
                    print(liste[i])
                ```
                
                * avec la syntaxe `#!python for i in range(debut, fin)`,
                la variable `i` aura pour 1^re^ valeur `debut`, puis `debut + 1`, 
                puis `debut + 2`, etc. jusqu'à `fin - 1` (la valeur `fin`
                est toujours **exclue**) ;
                
                * enfin, **la syntaxe la plus complète est** `#!python for i in range(debut, fin, pas)`.
                La variable `i` aura pour 1^re^ valeur `debut`, puis `debut + pas`, 
                puis `debut + 2*pas`, puis `debut + 3*pas`, etc. jusqu'à
                `fin - 1` (la valeur `fin` est toujours **exclue**). Les
                valeurs successives de `i` « sautent » donc de `pas` en
                `pas`.
                
        
        * parcourir **toutes** les positions et ne s'intéresser qu'aux **positions
        paires**. Il faudra donc effectuer un _test de parité_ pour chaque
        position rencontrée.
        
            ??? done "Les tests en Python (cas particulier ici, avec un test de parité)"
            
                Les tests en Python utilisent la syntaxe suivante :
                ```python
                if <test>:
                    ...         # Autant d'instructions que nécessaire
                elif <autre_test>:
                    ...         # D'autres instructions adaptées à la situation
                else:
                    ...         # Instructions si aucun test n'est valide
                ```
                
                Le but ici est de vérifier qu'un nombre (la position qu'on
                observe dans une liste, également appelée « indice ») est
                **pair** : cela revient au même de dire que le reste de
                sa division _entière_ par 2 est nul (on parle aussi de
                _division euclidienne_). En Python, l'opérateur de division
                entière est `//`, alors que le **reste** d'une division
                entière s'obtient avec l'opérateur `%`.
                
                **Exemple :** $14 = 3 \times 4 + 2$. Avec Python :
                
                ```python
                >>> 14 // 3
                4           # Le quotient ENTIER de 14 par 3
                >>> 14 % 3
                2           # Le reste de la division ENTIÈRE de 14 par 3
                ```
                
                Tester la parité de la variable `i` se fera donc en vérifiant
                si le reste de la division **entière** de `i` par 2 est
                **nul**. En Python, cela donne :
                ```python
                if i % 2 == 0:  # Notez le == ici, puisque = sert à l'affectation
                    ...         # Instructions à exécuter si
                    ...         # le test est validé !
                ```
                
                <center>**:warning: On teste l'égalité avec l'opérateur `==` :warning:**  
                _En effet, l'opérateur `=` sert déjà à affecter une valeur
                à une variable !_</center>
                
                Remarquez que les instructions `#!python elif` et `#!python else`
                sont _optionnelles_ : on ne les met **que** si on en a vraiment
                besoin.
    
    2. Récupérer les abscisses et ordonnées de chaque sommet (rappel : dans
    la liste `coordonnees_sommets`, on trouve d'abord l'abscisse du 1^er^
    sommet de la figure suivie de son ordonnée, puis l'abscisse du 2^nd^
    sommet suivie et de son ordonnée, et ainsi de suite jusqu'au dernier
    sommet de la figure). On doit donc savoir comme accéder aux éléments
    d'une liste Python lorsqu'on connaît leurs positions.
    
        ??? done "Accéder aux éléments d'une liste Python"

            * **Une liste Python est une collection de valeurs rangées selon
            leurs positions, comptées à partir de 0.** Ainsi, lorsqu'on
            définit la variable `#!python liste = [10, 20, 30]`, la valeur
            `#!python 10` est à la position `0`, la valeur `#!python 20`
            est à la position `1` et la valeur `#!python 30` est à la position
            `2`.
            
            * **Obtenir une valeur _particulière_ nécessite de connaître
            sa position :** l'instruction `#!python valeur = liste[2]`,
            lorsqu'elle est exécutée, affecte la valeur `#!python 30` à
            la variable `#!python valeur` (la variable `liste` n'est pas
            modifiée).

    
    3. Convertir ces paires de coordonnées en nombre complexe : cela, on
    sait déjà le faire, grâce à l'instruction `#!python complex(..., ...)`
    de Python.
    
    4. Modifier la liste `coordonnees_sommets` pour actualiser les coordonnées
    des sommets.
    
        ??? done "Modifier les éléments d'une liste Python"

            * **Modifier une valeur _particulière_ nécessite également de connaître sa
            position :** l'instruction `#!python liste[2] = 0`, lorsqu'elle est exécutée,
            **modifie** `liste` qui désigne alors `#!python [10, 20, 0]`.
            
            * Les listes de Python sont des objets _dynamiques_ : il est possible
            d'ajouter des éléments directement avec la _méthode_ `#!python append()`. Ainsi,
            l'instruction `#!python liste.append(1000)`, exécutée juste après la modification
            précédente, altère `liste` qui désigne alors `#!python [10, 20, 0, 1000]`.

Voici le code d'une fonction Python appelée `#!python modifier_coordonnees(...)`.
Elle prend en paramètres **deux** nombres complexes :

* `zT` en premier, qui gouverne les translations ;
* `zT` en second, qui contrôle les rotations-dilatations.

Lisez attentivement son code (et, plus particulièrement les commentaires
qui y figurent) afin de bien comprendre son fonctionnement.

```python
def modifier_coordonnees(zT, zR):
    for position in range(0, len(coordonnees_sommets), 2):
        # Chaque valeur de la variable « position » donnera accès dans la
        # liste « coordonnees_sommets » à une abscisse ; de même, on y
        # trouvera une ordonnée à chaque valeur de « position_suivante »
        position_suivante = position + 1
        partie_reelle = coordonnees_sommets[position]
        partie_imaginaire = coordonnees_sommets[position_suivante]
        z = complex(partie_reelle, partie_imaginaire)
        z = z + zT          # On effectue la translation associée au complexe zT
        z = z * zR          # On effectue la rotation associée au complexe zR
        nouvelle_partie_reelle = z.real     # La nouvelle abscisse du sommet
        nouvelle_partie_imaginaire = z.imag # La nouvelle ordonnée du sommet
        # On mémorise les nouvelles coordonnées dans la liste qui définit la figure
        coordonnees_sommets[position] = nouvelle_partie_reelle
        coordonnees_sommets[position_suivante] = nouvelle_partie_imaginaire
        # On demande au canevas de Tkinter de prendre en compte les nouvelles
        # coordonnées des sommets de la figure
        c.coords(bateau, coordonnees_sommets)   # ATTENTION ! Pour d'anciennes
        # versions de Python (par exemple la 3.4), il faudra écrire :
        # c.coords(bateau, *coordonnees_sommets)    # Notez l'étoile AVANT le nom
        # de la variable contenant la suite des coordonnées définissant la figure
```

??? question "Explications"

    * Ligne 2, on effectue un parcours de la liste par positions **paires**
    (on commence à la position `#!python 0` et on « saute » de `#!python 2`
    en `#!python 2` à chaque passage dans la boucle `#!python for`). Ainsi,
    `#!python coordonnees_sommets[position]` donnera l'_abscisse_ d'un sommet,
    qui sera également la _partie réelle_ de l'affixe de ce sommet.
    
    * Ligne 6, on calcule la position suivante, à laquelle on trouvera donc
    l'ordonnée du sommet qui nous intéresse dans ce tour de boucle.
    `#!python coordonnees_sommets[position_suivante]` donnera cette _ordonnée_,
    qui sera également la _partie imaginaire_ de l'affixe associée.
    
    * Ligne 9, on fabrique l'affixe en question à partir des informations
    précédemment récupérées.
    
    * Ligne 10, on effectue une translation qui dépend de la valeur de `#!python zT`.
    
    * Ligne 11, on effectue une rotation-dilatation qui dépend de la valeur
    de `#!python zR`.
    
    * Lignes 11 et 12, on récupère les nouvelles coordonnées du sommet.
    
    * Lignes 15 et 16, on mémorise ces nouvelles coordonnées en actualisant
    la liste `coordonnees_sommets`.
    
    * Ligne 19, enfin, on demande au canevas Tkinter de **modifier** la
    figure. Dans l'instruction `#!python c.coords(bateau, coordonnees_sommets)`,
    la variable `#!python bateau` est définie ailleurs dans le code, au
    moment où la figure est construite pour la première fois dans le canevas.
    Cette construction renvoie une valeur qui permet ensuite de l'identifier
    dans le canevas, afin de pouvoir la modifier par la suite.  
    Et c'est exactement ce que l'on fait en donnant à la _méthode_ `#!python coords()`
    de l'object canevas `#!python c` le numéro de notre figure et une nouvelle
    liste `#!python coordonnees_sommets` de coordonnées (« méthode » est
    le nom que l'on donne aux fonctions dans un contexte de Programmation
    Orientée Objets).
    
    ??? warning "Remarque technique utile avec de vieilles versions de Python"
    
        Avec une version telle que la 3.4, par exemple, la _méthode_
        ```#!python Canvas.coords(...)``` était prévue pour fonctionner
        avec la suite de coordonnées venant immédiatement après la valeur
        désignant la figure du canevas qu'on veut modifier (ici, cette valeur
        est mémorisée _via_ la variable `bateau`) :
        ```#!python c.coords(bateau, x1, y1, x2, y2, ...)``` (notez l'absence
        des crochets délimitant une **liste** Python). Sur des versions
        plus récentes, l'utilisation directe d'une liste est possible. Forcer
        « l'extraction » des valeurs d'une liste se fait à l'aide de la
        syntaxe :  
        ```python
        c.coords(bateau, *coordonnees_sommets)      # Notez l'étoile AVANT le nom
        # de la variable contenant la suite des coordonnées définissant la figure
        ```
        On appelle cette technique l'[_unpacking_](https://www.docstring.fr/glossaire/unpacking/){target='__blank' rel='noopener'}.

**Vous aurez remarqué que la fonction `#!python modifier_coordonnees(..., ...)`
prend DEUX paramètres.** Or chacune des fonctions que nous avons écrites
et associées à une touche ne fabrique qu'**un seul** nombre complexe. Il
va donc falloir systématiquement en fabriquer un deuxième, qui n'ait **aucune
action malvenue :**

* quand on souhaite translater une figure, on ne veut pas lui faire subir
de rotation-dilatation. Ainsi, le nombre complexe `#!python zR` qu'on doit
utiliser ne doit rien changer à la taille ou aux angles de la figure ! Son
module doit donc valoir `#!python 1` et son argument `#!python 0` :
$z_R = \big[ 1 \,; 0 \big] = 1$, tout simplement, donc `#!python zR = complex(1, 0)` ;

* de même, lorsqu'on souhaite faire subir une rotation-dilatation à la figure,
on ne souhaite pas la translater. Ainsi, le nombre complexe `#!python zT`
qu'on doit utiliser doit correspondre à une translation de vecteur nul
$\overrightarrow{0}(0\,;0)$. Donc $z_T = 0 + 0\mathbf i = 0$ et
`#!python zT = complex(0, 0)`.

Voici donc les dernières modifications à apporter à notre programme :

```python
def vers_la_gauche(evenement):
    zT = complex(-10, 0)    # On diminue la partie réelle de 10 pixels
    zR = complex(1, 0)      # Aucune rotation-dilatation
    modifier_coordonnees(zT, zR)

def augmenter_taille(evenement):
    zT = complex(0, 0)      # Aucune translation
    zR = rect(1.1, 0)       # On augmente le module de 10%
    modifier_coordonnees(zT, zR)
```

!!! note "À faire vous-même"

    === "Votre objectif"

        Modifiez les 6 autres fonctions de votre script d'une façon similaire
        à ce qui vient de vous être montré.

    === "Indication"

        Il n'y en a pas besoin. Les fonctions modifiées, données en exemple
        ci-dessus, doivent vous suffire.

    === "Solution"

        Non, vraiment, si vous venez ici avant d'avoir _sérieusement_ essayé
        par vous-même, vous être trompé et avoir cherché à corriger vos erreurs,
        vous ne jouez pas le jeu sainement et intelligemment.
        
        Vous contenter de copier-coller du code sans vous efforcer de le comprendre
        ne vous servira jamais à rien... Mais bon, c'est votre vie, hein :confused: !
        
        ```python
        def vers_la_gauche(evenement):
            zT = complex(-10, 0)    # On diminue la partie réelle de 10 pixels
            zR = complex(1, 0)      # Aucune rotation-dilatation
            modifier_coordonnees(zT, zR)

        def vers_la_droite(evenement):
            zT = complex(10, 0)     # On augmente la partie réelle de 10 pixels
            zR = complex(1, 0)      # Aucune rotation-dilatation
            modifier_coordonnees(zT, zR)

        def vers_le_haut(evenement):
            # Ne pas oublier que l'axe des ordonnées est orienté VERS LE BAS !
            zT = complex(0, -10)    # On diminue la partie imaginaire de 10 pixels
            zR = complex(1, 0)      # Aucune rotation-dilatation
            modifier_coordonnees(zT, zR)

        def vers_le_bas(evenement):
            zT = complex(0, 10)     # On augmente la partie imaginaire de 10 pixels
            zR = complex(1, 0)      # Aucune rotation-dilatation
            modifier_coordonnees(zT, zR)

        def augmenter_taille(evenement):
            zT = complex(0, 0)      # Aucune translation
            zR = rect(1.1, 0)       # On augmente le module de 10%
            modifier_coordonnees(zT, zR)

        def diminuer_taille(evenement):
            zT = complex(0, 0)      # Aucune translation
            zR = rect(0.9, 0)       # On diminue le module de 10%
            modifier_coordonnees(zT, zR)

        def rotation_a_gauche(evenement):
            zT = complex(0, 0)      # Aucune translation
            zR = rect(1, 0.1)       # On augmente l'argument de 0.1 radian
            modifier_coordonnees(zT, zR)

        def rotation_a_droite(evenement):
            zT = complex(0, 0)      # Aucune translation
            zR = rect(1, -0.1)      # On diminue l'argument de 0.1 radian
            modifier_coordonnees(zT, zR)
        ```


## Le programme complet

Ne dépliez le bloc ci-dessous que si vous êtes complètement bloqués depuis
un bon moment. Il est _vraiment_ important, en programmation, de commettre
des erreurs, d'essayer de les comprendre et de les corriger. Se rabattre
trop rapidement, ou pire encore trop _systématiquement_ sur du copier-coller
**limitera _très fortement_** votre progression, voire vous empêchera de
progresser.

??? done "Le programme complet"

    === "Le code"

        **Remarque :** ce programme a été légèrement modifié par rapport à ce que
        vous venez de lire, afin de le rendre plus court.

        ```python
        --8<-- "docs/codes/complexes-et-animations-v6.py"
        ```
    === "Le rendu"
    
        ![](images/animation-1.gif)

??? note "Pour aller plus loin"

    === "Quelques défis"

        Voici quelques propositions d'améliorations que vous pourriez (ou pas)
        souhaiter apporter.
        
        1. **Informatique :** modifier le code de la fonction `#!python modifier_coordonnees(...)`
        afin d'utiliser la version la plus basique de l'instruction `#!python range`
        et un test de parité pour se repérer dans le parcours de boucle.
        
        2. **Mathématique :** faire en sorte que la rotation puisse se faire
        avec un centre différent (en l'état, le centre est _toujours_ le point
        $O$, origine du repère, qui est donc situé dans le coin supérieur gauche
        du canevas Tkinter).
        
        3. **Informatique et mathématique :** rendre visible à l'écran et déplaçable le centre
        de la rotation de la figure.
        
        4. **Informatique :** proposer à l'aide de recherches personnelles un
        code encore plus concis, en utilisant les _valeurs par défaut_ qu'on
        peut donner aux paramètres des fonctions.

        [Ce](codes/complexes-et-animations-v7.py) code répond à tous ces défis,
        mais aucune explication n'est donnée ! Notez que j'y utilise des
        fonctionnalités plus avancées de Python, et que si vous voulez les
        comprendre, il vous faudra fournir un effort significatif. Le
        [livre libre sur Python de G. SWINNEN][swinnen] pourra vous aider !
    
    === "Le rendu final"
    
        ![](images/animation-2.gif)
    
[swinnen]: https://inforef.be/swi/download/apprendre_python3_5.pdf
