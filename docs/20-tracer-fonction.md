# Tracer des fonctions avec Python 3 et Tkinter

## Introduction

Tkinter est la blibliothèque de composants graphiques disponible par défaut avec
Python 3. Vous pourrez trouver une traduction en français de sa documentation
à l'adresse [http://tkinter.fdex.eu/][tkinter-doc]{target='__blank' rel='noopener'}.

Vous pourrez également trouver une introduction plus complète à la logique
du fonctionnement de Tkinter dans le [document que je propose aux élèves
de la spécialité NSI](https://trieles.gitlab.io/nsi/20-tkinter/){__target=blank}.

!!! warning "Recommandations importantes"

    * Au début de ce document, qui vise à vous faire découvrir quelques notions
    de base de Tkinter, il vous sera demandé de travailler dans une **console
    interactive** (Idle ou iPython). Par la suite, vous pourrez créer des scripts.

    * L'intérêt du mode interactif est de pouvoir **observer les effets successifs
    de chaque instruction** (en mode script, cela va tellement vite qu'on a
    l'impression que tout se passe _instantanément_ et _simultanément_). En
    « mode interactif », la construction de l'interface graphique (souvent abrégée
    en **GUI**, pour *<b>G</b>raphical <b>U</b>ser <b>I</b>nterface* en anglais)
    est _dynamique_ et _progressive_, l'observer étape après étape est instructif.

    * Je vous demande de faire en sorte que ce document occupe une moitié de
    votre écran, que votre console interactive occupe le quart supérieur de
    l'autre partie de l'écran, et de veiller à ce que les « fenêtres Tkinter »
    que vous fabriquerez soient toujours visibles sur l'espace restant de
    l'écran (vous devrez probablement les déplacer, *à chaque fois*).
    
    * Dans la suite, les lignes de codes qui doivent être saisies en console
    sont systématiquement précédées des 3 chevrons `>>>`, afin d'insister
    sur l'utilisation de ce mode interactif. **Faites bien attention, en
    utilisant le bouton facilitant la copie du code :** les chevrons seront
    _aussi_ copiés ! _Il faudra donc les enlever._
    
        ??? faq "Mais pourquoi compliquer les choses avec ces chevrons `>>>` ?"
        
            Parce qu'être efficace avec un clavier est une compétence utile,
            de nos jours. Comme dans beaucoup de domaines, l'important est de
            pratiquer pour progresser... plus vous utiliserez le clavier, plus
            vous serez à l'aise avec, et le copier-coller à la souris constitue
            donc, de ce point de vue, une sérieuse entrave à vos progrès !

??? warning "Rappels"

    Idle est l'éditeur minimaliste « livré » par défaut avec Python. Il
    dispose de deux modes de fonctionnement :
    
    * **le mode interactif**, dans lequel chaque ligne commence par
        trois _chevrons_ : >>>. Dans ce mode, on saisit les instructions
        les unes après les autres, séparées par un appuis sur la touche
        ++"Entrée⏎"++. Elles sont exécutées au fur et à mesure : un
        appui sur la touche ++"Entrée⏎"++ déclenche l'exécution de l'instruction
        correspondante - si celle-ci est complète !
        
        Si elle n'est pas complète, Idle passez à la ligne (avec un décalage
        horizontal - c'est ainsi que Python définit la _portée_ des blocs d'instructions).

        
    * **le mode script.** On y accède en cliquant sur le menu ++"File"++
        puis sur l'entrée ++"New File"++ (ou en pressant simutanément
        les touches ++ctrl+n++). Un nouveau fichier vide est alors créé,
        dans lequel on peut écrire plusieurs instructions, sur plusieurs
        lignes. Dans cette situation, un appui sur la touche ++"Entrée⏎"++
        ne déclenche pas l'exécution de l'instruction qui précéde : l'éditeur
        nous fait juste passer à la ligne suivante.
            
        Pour éxécuter un script dans ce mode, on appuiera sur la touche ++f5++,
        ou on ira dans le menu ++"Run"++ cliquer sur l'entrée ++"Run Module"++
        (ce qui revient au même !).

    **Le mode interactif dispose d'une fonctionnalité bien pratique !**
        Lorsqu'on utilise le *shell* (la désignation anglaise pour la console
        interactive), on peut se déplacer dans les lignes déjà saisies à l'aide
        de la souris ou des touches ++"↑"++ et ++"↓"++ du clavier (cela permet
        d'avoir accès à l'historique des instructions). On dit que la
        ligne sur laquelle on se positionne ainsi « a le *focus* ».
        
    Ensuite, **lorsqu'on appuie sur la touche ++"Entrée⏎"++ du clavier,
    les instructions de la ligne ayant le _focus_** sont _automatiquement_
    et _intégralement_ recopiées sur la dernière ligne de la console
    (la prochaine à pouvoir être exécutée). On gagne ainsi du temps
    en évitant de re-saisir une instruction déjà saisie. Cela facilite
    également la modification d'une instruction erronée : on rappelle
    ainsi le code, et on le modifie avant d'exécuter la version modifiée...


## Premiers pas

Chaque fois que l'on voudra exploiter la bibliothèque Tkinter, il faudra commencer
par l'importer (on va faire ici un import global, bien que cela ne soit pas optimal).


```pycon
>>> from tkinter import *               # Import indispensable (mais excessif).
>>> f=Tk()                              # Création d'une fenêtre.
```

Normalement, une fenêtre (très très vide) a dû apparaître (si vous ne la
repérez pas au premier coup d'œil, cherchez-là dans la « barre des tâches »
ou le *dock*).

**:warning: NE LA FERMEZ SURTOUT PAS ! :warning:**

Si par malheur vous avez déjà cliqué sur la croix qui, traditionnellement, ferme
les fenêtres, vous n'avez plus qu'à saisir à nouveau les lignes 1 et 2 précédentes...

??? done "Astuce pour éviter de « trop retaper » en mode interactif !"

    Cela a déjà été indiqué dans les « Rappels importants » précédents :
    Idle (l'éditeur minimaliste « livré » par défaut avec Python) dispose
    d'une fonction bien pratique, lorsqu'on utilise son *shell* (la désignation
    anglaise pour la console interactive). On peut se déplacer dans les
    lignes déjà saisies à l'aide de la souris ou des touches ++"↑"++ et
    ++"↓"++ du clavier, afin de revoir un code précédent : on dit que la
    ligne sur laquelle on se positionne ainsi « a le *focus* ». Ensuite,
    **lorsqu'on appuie sur la touche ++"Entrée⏎"++ du clavier, les instructions
    de la ligne ayant le _focus_** sont _automatiquement_ et _intégralement_
    recopiées sur la dernière ligne de la console (la prochaine à pouvoir
    être exécutée). On peut ainsi facilement recopier un code et le modifier
    (si nécessaire) avant de le ré-exécuter.


??? info "Remarques"

    * Les commentaires dans le code sont à lire avec attention ! Il est en revanche
    inutile de les taper :wink:...

    * La <b>P</b>rogrammation <b>O</b>rientée <b>O</b>bjet (POO) est le style
    de programmation de prédilection pour les interfaces graphiques (au moins
    en Python). Vous aurez donc très souvent à faire appel aux notations spécifiques
    à la POO : `obj.meth(...)`, ce qui signifie que vous faites appel à la
    « fonction » nommée `meth` d'un « objet » nommé `obj`. Les fonctions
    ont un nom un peu particulier, en POO, puisqu'on les appelle *méthodes*.
    De même, une variable `var` rattachée à un objet `obj` sera désignée par
    `obj.var`, et on la qualifiera alors d'*attribut*. 

    ??? summary "Pour parler (un rien) de Programmation Orientée Objet"

        Pour faire simple, un **objet** est une portion de code *autonome*. Il est défini
        à partir d'une **classe**, qui, chaque fois qu'elle est **instanciée** (exécutée),
        crée un nouvel objet.
        
        Pour tenter une analogie : une classe est un moule à gâteau, un objet est un
        gâteau, et on peut créer plusieurs gâteaux (qui peuvent être un peu différents)
        à partir du même moule.
        
        Un **attribut** est une caractéristique de l'objet. C'est une variable qui n'existe
        que lorsque un objet est instancié, à laquelle on ne peut accéder qu'à travers
        l'objet, et qui est liée à cet objet en particulier (deux objets instanciés à
        partir de la même classe auront chacun un même attribut, qui portera le même
        nom, mais désignera des valeurs différentes.
        
        Une **méthode** est une fonction « embarquée » dans l'objet, qui peut agir
        sur lui « de l'intérieur ». Elle peut par exemple modifier les valeurs des
        attributs. On peut appeler une méthode à partir d'un objet avec la syntaxe
        `objet.methode()`


??? faq "Pour aller plus loin"

    On peut évidemment contrôler le « format  »  de la fenêtre : définir sa
    taille, décider si l'utilisateur est autorisé à la redimensionner (et si
    oui, dans quelle direction).

    ```pycon
    # Altérer la géométrie d'une fenêtre
    >>> f.geometry('200x30')
    # L'approche suivante est meilleure
    >>> f.geometry('{}x{}'.format(300, 50))
    ```

    On peut aussi lui donner un titre :

    ```pycon
    >>> f.title('Grapheur')
    ```


??? warning "Spécificité d'un programme « graphique »"

    Dans un script, il faudra systématiquement démarrer la « boucle principale ».
    En clair, cela signifie que la **dernière** instruction de votre scripts
    sera `f.mainloop()` (où `f` désigne la _fenêtre principale_ du programme).

    La méthode `mainloop` provoque le démarrage du **gestionnaire d'événements**
    associé à la fenêtre : il est indispensable au bon fonctionnement de votre
    application graphique. Concrètement, une **boucle infinie** est démarrée, elle
    ne sert qu'à « consommer » une quantié minimale des ressources de la machine,
    juste ce qu'il faut pour surveiller très régulièrement (et très très très
    souvent :wink! !) les clics de souris, les pressions sur les touches du
    clavier, etc.

    C'est un peu cette instruction qui « met en marche » votre application graphique,
    en lui permettant d'interagir avec l'utilisateur.

    **On parle dans cette situation de « programmation événementielle »**, dans
    laquelle une boucle infinie attend sans arrêt qu'on la sollicite pour réagir,
    par opposition à la **« programmation séquentielle »** dans laquelle un programme
    a vocation à se terminer à un moment (qui, si l'on s'en donnait la peine, pourrait
    être) connu d'avance.

    Programme séquentiel       |  Programme événementiel
    :-------------------------:|:-------------------------:
    ![Programme séquentiel](images/deroulement-programme-sequentiel.png){width=116} | ![Programme événementiel](images/deroulement-programme-evenementiel.png){width=500}
    
    Les images proviennent de l'excellent livre libre « [Apprendre à programmer avec Python 3][swinnen]{target='__blank' rel='noopener'} ».
    Bien qu'un peu ancien, désormais, il constitue néanmoins une ressource
    de première importance !


## Un premier *widget* : le `Canvas`

Un *widget* est un *<b>WI</b>ndow ga<b>DGET</b>*. Une interface graphique est
composée de divers *widgets*. Nous ne verrons ici que le `Canvas`, qui sert
à dessiner (mais vous pourrez en découvrir d'autres par vous-mêmes, notamment
en explorant la [documentation de Tkinter][tkinter-doc]{target='__blank' rel='noopener'}
ou mon [introduction à Tkinter][tkinter-intro]{target='__blank' rel='noopener'}).

En supposant que vous avez saisi, dans votre console Python, les instructions
précédentes et que vous n'avez pas fermé la fenêtre qui a dû apparaître, saisissez
maintenant le code suivant :

```pycon
# Création d'un label (une zone d'affichage), dépendant de la fenêtre f
>>> c = Canvas(f)
```

??? tip "Pour aller plus loin : créer _et_ configurer un `Canvas` en une seule instruction"

    Si l'on veut un canevas ayant une certaine taille et une couleur de
    fond qui permette de bien le différencier du reste de la fenêtre, on
    peut saisir la commande :
    
    ```pycon
    >>> c = Canvas(f, width=600, height=400, bg="light yellow")
    ```

**:warning: Mais pourquoi donc rien ne s'affiche :thinking: ?**

C'est simple ! En fait, **le *widget* n'est pas encore *positionné* dans
la fenêtre.** Il y a en effet plusieurs façons de placer les éléments constitutifs
de l'interface graphique au sein de la fenêtre principale (qui ici est désignée
par la variable `f`).

Nous utiliserons le gestionnaire de positionnement `pack` : c'est le plus
simple, donc le plus basique, mais aussi le plus facile à utiliser (il
« empile »  les *widgets* l'un sous l'autre, sur une seule colonne ― on
peut aussi choisir de mettre chaque _widget_ l'un à côté de l'autre, en
ligne).

??? info "Remarque"
    
    **Tkinter dispose de 3 gestionnaires de positionnement :** `pack`, `grid`
    et `place`. Pour des interfaces graphiques plus complexes, on utilisera
    plutôt `grid`.     Vous trouverez plus de détails dans [ce document][tkinter-intro]{target='__blank' rel='noopener'}
    (mais ça n'est pas une priorité, si le sujet vous intéresse, vous lirez
    ce didacticiel chez vous, sur votre temps libre).

```pycon
# Insertion pour affichage du label dans la fenêtre f
>>> c.pack()
# Note : la géométrie de la fenêtre s'en trouve affectée !
```

??? faq "Changer la configuration du *widget* après sa création"

    <a name="postconfig"></a>
    Si l'on n'est pas satisfait de la configuration par défaut du `Canvas`,
    ou qu'on a oublié de choisir les bons réglages lors de l'*instanciation*
    du `Canvas` (*instanciation* est encore un terme propre à la POO), on peut
    *a posteriori* modifier la configuration d'un *widget* :

    ```pycon
    # Réglages a posteriori (on peut aussi décider de faire ces réglages lors de
    # la création du Canvas, c'est une question de point de vue et de lisibilité).
    >>> c.configure(background="light green")
    # Il y a une autre façon de faire (à vous de choisir) :
    >>> c['background'] = "light yellow"
    >>> c.configure(height="800")
    >>> c["width"] = 800
    # On peut aussi régler plusieurs paramètres en une seule instruction :
    >>> c.configure(width=600, height=400)
    ```


## Dessiner avec le `Canvas` !

Le *widget* `Canvas` permet donc de dessiner : on peut y placer des **éléments
graphiques** tels que des lignes, des polygones, des images, etc. La documentation
complète de l'objet `Canvas` de Tkinter est disponible [ici][canvas]{target='__blank' rel='noopener'}.

**On suppose dans la suite que vous avez créé un canevas de 600 pixels de
large par 400 pixels de haut** (reportez vous au bonus
« [Changer la configuration du *widget* après sa création](#postconfig) »
si nécessaire).


### Créer une ligne brisée

Le canevas est un objet Python possédant de nombreuses méthodes, grâce auxquelles
il peut notamment dessiner de nombreux éléments graphiques (là encore, la
[documentation][tkinter-canvas-items]{target='__blank' rel='noopener'} vous
apportera quantité d'informations complémentaires, mais _plus tard_ : ça
**n**'est **pas** l'objectif du moment !). 

La commande pour créer des lignes droites est :

```pycon
>>> ligne = c.create_line((0,0, 100, 300, 200, 100, 400, 0), fill="red", width=5)
```

**:astonished: Que remarquez-vous ? La ligne ainsi dessinée est-elle
conforme à vos attentes ?**

!!! warning "Dans un canevas, l'axe des ordonnées est dirigé _VERS LE BAS_ !"

??? note "Note historique"

    Cette situation n'est pas propre à Python / Tkinter. En fait, dans la
    plupart des langages de programmation :
    
    * l'origine $O(0\,;0)$ du repère est le coin supérieur gauche de l'écran
    (plus exactement : de la fenêtre ou du canevas) ;
    
    * l'axe des abscisses $(Ox)$ est dirigé comme on en a l'habitude (de
    la gauche vers la droite, en suivant le bord supérieur de la fenêtre
    ou du canevas, comme en mathématiques) ;
    
    * **mais l'axe des ordonnées $(Oy)$ est dirigé vers le bas**, en sens *contraire*
    du sens habituel (en suivant le bord gauche de la fenêtre ou du canevas).
    
    Les raisons à cette situation qui pourrait vous surprendre sont purement
    historiques : sur les premiers afficheurs (des moniteurs analogiques,
    ressemblant aux anciennes télévisions à tube cathodique), le faisceau
    d'électrons partait du coin supérieur gauche, se déplaçait vers la droite,
    puis descendait légèrement en revenant à gauche. La vitesse à laquelle
    les premiers écrans pouvaient dessiner les caractères était assez lente,
    il était donc préférable de suivre le sens de lecture... *occidental*
    (n'oublions pas que l'[ordinateur est, pour l'essentiel, une invention
    anglo-saxonne][ordinateur]{target='__blank' rel='noopener'}, même si
    l'histoire de [Konrad ZUSE][konrad-zuse]{target='__blank' rel='noopener'}
    mérite d'être connue).

    Cf. [:gb: cette question sur StackOverflow][stack-oy-bas]{target='__blank' rel='noopener'}
    ainsi que [:gb: cet article de Wikipedia qui traite de l'histoire de la télévision
    analogique][analog-tv]{target='__blank' rel='noopener'}.
    
    Voici une animation (issue de Wikipedia) montrant comment une [image
    *entrelacée*][entrelacement]{target='__blank' rel='noopener'} était
    affichée sur un moniteur analogique.
    
    ![Comment une image est affichée sur une télévision analogique](images/CRT_image_creation_animation.gif){width=200}
    
    On voit bien que l'affichage débute en haut à gauche de l'écran, puis
    que le balayage se fait vers la droite et vers le bas...


??? info "Remarques"

    * On a conservé le **numéro** de l'élément graphique ainsi créé dans une
    variable nommée `l`. On peut ensuite, à l'aide de ce numéro, modifier l'élément
    graphique correspondant. On peut notamment le supprimer :
        
        ```pycon
        >>> c.delete(ligne)
        ```

    * On a précisé d'emblée certains paramètres de l'élément graphique, ici
    sa couleur (option `fill="red"`) et l'épaisseur du trait (option `width=5`).
    Mais si on ne l'a pas fait (oubli... ou flemme :wink: ?), grâce au numéro
    mémorisé _via_ la variable `ligne`, on peut modifier _a posteriori_
    l'élément graphique :

        ```pycon
        >>> c.itemconfig(ligne, fill="blue", width=10)
        ```

### Comment générer les points nécessaires à tracer une fonction ?

Une calculatrice (ou un logiciel permettant de tracer les courbes représentatives
de fonctions) se contente d'afficher un (grand) nombre de points de la courbe,
 et de les relier par des (petits) segments de droites.

Pour appliquer le même principe ici, on va donc devoir générer une suite
d'abscisses et d'ordonnées : $x_1$, $f(x_1)$, $x_2$, $f(x_2)$, ... , $x_N$, $f(x_N)$
($N$ devant être choisi et fixé avant de débuter les calculs).

 **Pour que Python stocke ces valeurs, on va utiliser une variable de type
`list`.**

!!! warning "C'est le moment où l'on passe en mode « script » !"

    Dans Idle, pressez-donc simutanément les touches ++ctrl+n++ : une nouvelle
    fenêtre s'ouvre. Disposez-là par-dessus la console interactive. Vous
    enregistrerez le code à venir dans un fichier nommé `traceur_fonction.py`.
    
    **N'oubliez pas que la première ligne de ce script devra être :**
    
    ```python
    from tkinter import *
    ```
    
    ...et que la **dernière ligne** devra être :
    ```python
    f.mainloop()
    ```

Mais il nous faut d'abord définir une fonction. Voici un exemple, à titre
de rappel :

```python
def f(x):
    return 11/3000*(x-100)**2+50
```   

Cela permet à Python de savoir calculer des images par la fonction $f$
définie par $f(x)=\dfrac{11}{3000}(x-100)^2+50$.

Pour définir une liste, il y a deux méthodes :

* **la liste en compréhension**. Par exemple, pour définir un ensemble
    d'abscisses :
    ```python
    xs = [ x for x in range(-100, 700, 50) ]    # Parce que pourquoi pas ?
    ```
    
    ??? faq "Comprendre la notation « en compréhension »"
        
        L'instruction précédente se lit, presque « mot à mot », de la façon
        suivante :
        
        * « Python, crée-moi une liste... » (qui correspond au crochet ouvrant
        `[`) ;
        
        * « ...composée des valeurs `x`... » (qui correspond au `x`) ;
        
        * « ...pour x décrivant l'ensemble des entiers compris entre `-100`
        inclus et `700` exclus en sautant de `50` en `50`... » (qui correspond
        au `for x in range(-100, 700, 50`) ;
        
        * et enfin le crochet fermant `]` pour cloturer la liste.
        
        Le fait qu'on écrive l'instruction (presque) comme on peut la penser
        et la décrire de vive voix a amené à l'expression « liste en compréhension »,
        car on a l'impression que Python « comprend » ce qu'on souhaite
        qu'il nous fabrique.
    
    On peut de même définir la liste des ordonnées, en faisant varier
    une variable dans la liste des abscisses précédemment générées :
    ```python
    fxs = [ f(x) for x in xs ]
    ```
    
    ??? info "Remarque"
    
        Python sait nativement parcourir une liste : l'instruction `for x in xs`
        va amener la variable `x` a prendre _successivement_ toutes les
        valeurs contenues dans la liste `xs`.
    
    
    :warning: **Ici, cette technique n'est pas adaptée !** En effet, elle
    ne peut pas satisfaire aux contraintes posées par Tkinter, car pour
    dessiner des lignes dans un `Canvas`, il faut fabriquer une liste dans
    laquelle se succéderont abscisse et ordonnée de chaque point de la courbe...
    
    Mais la technique de « compréhension de liste » est très simple et très
    élégante — lorsqu'on en a pris l'habtiude... _Elle est en outre **explicitement
    au programme** des filières tehnologiques !_

* **la liste en extension**. Cette approche sera plus indiquée, pour nos
    besoins.
    
    * On commence par créer une liste vide.
    
    * On ajoute autant d'éléments que nécessaire grâce à une boucle. Les
    éléments s'ajoutent *par la droite*, à l'aide de la méthode `append(...)` :
    ```python
    cs = []
    for x in range((-100, 700, 50):
        cs.append(x)
        cs.append(f(x))
    ```

On peut ensuite tracer la courbe représentative de la fontion :

```python
C_f = c.create_line(cs, fill="red", width=5)
```


### Un programme complet

Le code que vous avez normalement dû obtenir est le suivant :

```python
from tkinter import *

def f(x):
    return 11/3000*(x-100)**2+50

cs = []
for x in range(-100, 700, 50):
    cs.append(x)
    cs.append(f(x))

f = Tk()
c = Canvas(f, width=600, height=400, background="light yellow")
c.pack()

C_f = c.create_line(cs, fill="red", width=5)

f.mainloop()
```

Et voici le résultat que vous devriez observer :

![Fonction tracée](images/fonction.png){width=500}

**:astonished: Que remarquez-vous ? La courbe obtenue correspond-elle à
vos attentes ?**

??? done "Explications"

    La fonction du second degré ayant un coefficient $a>0$ (ici, $a=\frac{11}{3000}$,
    les branches de la parabole devraient être orientées « vers le haut »,
    façon « sourire de _smiley_ :slightly_smiling_face: » !).
    
    C'est une nouvelle fois l'orientation de l'axe des ordonnées qui est
    en cause. Il va vraiment falloir solutionner ce problème...
    
    Lui et un autre : le voyez-vous ?
    
    ??? done "Explication additionnelle"
    
        L'autre problème est celui de l'échelle : ici, le 1 désigne 1...
        pixel ! Les graphiques obtenus risquent donc d'être tout à fait
        « rikikis » ! Modifiez le programme `traceur_fonction.py` de sorte
        à ce que la fonction tracée soit juste $f(x)=x^2$... Que dites-vous
        du résultat ? Lisible ?
        
        On va devoir appliquer un coefficient de proportionnalité aux abscisses
        et aux ordonnées afin de modifier l'échelle du graphique. En fait,
        on va devoir en applique *deux* :
        
        * *un en abscisse*, afin de pouvoir les « dilater », de sorte à ce
        qu'une unité représente, mettons, 10 pixels (ou 20, ou...) ;
    
        * *un autre en ordonnées*, **négatif** celui-là, afin de pouvoir à
        la fois dilater les ordonnées aussi **et** retrouver
        des courbes orientées dans « le bon sens » (au sens : « le sens
        auquel on est habitué » — cette histoire de sens met les sens à
        rude épreuve, « sens dessus-dessous », pourrait-on dire :wink: !).
            
        Mais cela ne suffira pas : il faudra travailler encore un peu si
        on veut « décaler » l'origine du repère, par exemple au centre du
        `Canvas` !


## Obtenir un tracé satisfaisant

On a plusieurs soucis à régler :

* décaler l'origine où on le souhaite ;

* gérer un facteur d'échelle, en abscisse comme en ordonnée.

**Il est important de comprendre qu'on doit gérer deux sortes
d'unités :**

* **l'unité _mathématique_**, qui est celle avec laquelle on effectue les calculs
d'images ;

* **l'unité _informatique_**, qui est celle avec laquelle on effectue les affichages
à l'écran.

Pour simplifier les choses, et faciliter les modifications ultérieures,
on va définir un certain nombre de variables additionnelles :

* **la variable `N`** désignera le nombre de points qu'on souhaite utiliser
pour représenter la courbe de la fonction `f`. Il sera dès lors aisé d'avoir
un tracé plus ou moins précis en jouant sur la valeur de `N`. Ainsi, avec
`N = 10` on construira une courbe à l'aide de 10 points, alors qu'on aura
100 points avec `N = 100` (la courbe sera alors plus « jolie », car plus
régulière, plus lisse) ;

* **les variables `L` et `H`** désigneront respectivement la largeur et
la hauteur du canevas ;

* **les variables `x_min`, `x_max`, `y_min`, et `y_max`** désigneront les
coordonnées _mathématiques_ « minimales et maximales » de la fenêtre dans
laquelle on cherche à inscrire le tracé de la courbe.

Je vous propose ci-dessous deux fonctions qui permettront de convertir une
coordonnée _mathématique_ en coordonnée _dans le canevas_. Ces fonctions
feront usage de deux variables utilitaires ainsi que de deux autres variables
désignant l'abscisse et l'ordonnée de l'origine _mathématique_ $O$ dans
le système de coordonnées du canevas (rappel : origine en haut à gauche,
axe des abscisses courant le long du bord supérieur du canevas et orienté
comme d'habitude, axe des ordonnées courant le long du bord droit du canevas
et orienté _vers le bas_) :
```python
# Coefficients de proportionnalité entre les dimensions
# du canevas informatique et celles de la fenêtre mathématique.
x_ratio = L / (x_max - x_min)   # En abscisse...
y_ratio = H / (y_max - y_min)   # ...et en ordonnée.

# Abscisse de l'origine O dans le repère informatique :
x_O_aff = abs(x_min) * x_ratio
  # Ordonnée de l'origine O dans le repère informatique :
y_O_aff = abs(y_max) * y_ratio
```

Une fonction transformera l'abscisse :

```python
def x_to_xaff(x):
    return x * x_ratio + x_O_aff
```

Une autre transformera l'ordonnée :

```python
def y_to_yaff(y):
    return -1 * y * y_ratio + y_O_aff
```

**Questions**

1. Comment générer l'ensemble des abscisses $x_i$ des points dont on aura
besoin pour tracer la courbe ? La solution précédemment utilisée **ne**
convient **pas**, car une instruction du type `[ x for x in range(x_min, x_max) ]`
aura plusieurs inconvénients :

    * il y aura une erreur si `x_min` ou `x_max` **ne** sont **pas** des
    nombres entiers (relatifs) ;
    
    * s'ils le sont, `range(...)` ne peut progresser que par « sauts » entiers
    (relatifs). Par défaut, la valeur du saut est `1`, mais il pourrait
    être `-1`, `2` ou `-10`. Il suffit de le préciser, par exemple :
    `for x in range(100, -100, -20)`.

    ??? faq "Indication"
    
        Réfléchissez d'abord !
        
        ??? info "Vous avez réfléchi ?"
        
            Et bien réfléchissez encore :stuck_out_tongue_winking_eye: !
            
            ??? done "« Sérieux, c'est relou ! »"
            
                OK, j'arrête de vous faire marner : partez de `x_min` et
                augmentez à chaque étape de `(x_max - x_min) / N`. Attention
                à la valeur d'arrêt !

2. Comment générer les points à l'aide d'une construction de liste en extension ?
On partira de la liste vite `cs = []` (`cs` comme « coordonnées » au pluriel).

Au final, la base de votre programme sera la suivante :

```python
from tkinter import *

N = 100         # Nombre de points pour le tracé de la courbe.
L = 800         # Largeur du canevas.
H = 800         # Hauteur du canevas.

x_min = -5      # La fenêtre d'affichage mathématique
x_max = 10      # pour le tracé de la courbe
y_min = -15     # de la fonction f
y_max = 35      # définie plus bas.

# Coefficients de proportionnalité entre les dimensions
# du canevas informatique et celles de la fenêtre mathématique.
x_ratio = L / (x_max - x_min)   # En abscisse...
y_ratio = H / (y_max - y_min)   # ...et en ordonnée.

# Abscisse de l'origine O dans le repère informatique :
x_O_aff = abs(x_min) * x_ratio
  # Ordonnée de l'origine O dans le repère informatique :
y_O_aff = abs(y_max) * y_ratio

# La fonction f dont on souhaite tracer la courbe :
def f(x):
    return -x**2+35

# Conversion d'une ABSCISSE MATHÉMATIQUE en ABSCISSE dans le CANEVAS.
def x_to_xaff(x):
    return x * x_ratio + x_O_aff

# Conversion d'une ORDONNÉE MATHÉMATIQUE en ORDONNÉE dans le CANEVAS.
def y_to_yaff(y):
    return -1 * y * y_ratio + y_O_aff

# Les coordonnées des points qui seront utilisés pour créer la courbe.
cs = []
for i in range(...):    # >>> COMPLÉTER LES POINTILLÉS !
    x = ...
    cs.append(...)
    cs.append(...)

# Le code pour l'interface graphique.
f = Tk()
c = Canvas(f, width=L, height=H, background="light yellow")
c.pack()
C_f = c.create_line(cs, fill="red", width=5)
# Pour enregistrer une image (vectorielle) du résultat :
c.update()
c.postscript(file="fonction.ps", colormode='color')
# Boucle principale de la fenêtre.
f.mainloop()
```

En travaillant davantage (cf. section suivante), vous pourriez obtenir le
résultat suivant, pour le tracé de la fonction $f$ définie par $f(x)=x^2$,
pour $x\in[-5\,;5]$ et $y\in[-1\,;26]$ :

![](images/fonction_axes.png){width=400}



## Pour aller plus loin

* On peut souhaiter améliorer certains points. Par exemple, avoir une fenêtre
(mathématique) de tracé qui serait auto-déterminée.

* Il serait bon de tracer les axes du repère ! Utilisez la
[documentation de Tkinter](http://tkinter.fdex.eu/doc/caw.html#lignes){target='__blank' rel='noopener'}
pour vous aider (il y a une option `arrow`, pour les lignes) !

* Et les graduations ? Vous avez pensé aux graduations :wink: ?

* Essayez de tout refaire avec des compréhensions de listes... C'est bon,
les compréhensions de liste, mangez-en !

[swinnen]: http://inforef.be/swi/python.htm
[tkinter-doc]: http://tkinter.fdex.eu/ "Documentation de Tkinter traduite en français"
[tkinter-intro]: https://trieles.gitlab.io/nsi/2-tkinter/ "Une introduction à Tkinter"
[grid-forget]: http://tkinter.fdex.eu/doc/gp.html#w.grid_forget
[pack-forget]: https://waytolearnx.com/2020/07/comment-afficher-ou-masquer-un-widget-tkinter-python.html
[stack-widget-forget]: http://stackoverflow.com/questions/3819354/in-tkinter-is-there-any-way-to-make-a-widget-not-visible
[canvas]: http://tkinter.fdex.eu/doc/caw.html "Widget Canvas de Tkinter (canevas, pour dessiner)"
[tkinter-canvas-items]: http://tkinter.fdex.eu/doc/caw.html#identification-des-items-graphiques "Les élements graphiques du Canvas de Tkinter"
[tkinter-canvas-items-id]: http://tkinter.fdex.eu/doc/caw.html#canvasidnum "Identifier un objet graphique du Canvas de Tkinter"
[tkinter-canvas-tag_lower]: http://tkinter.fdex.eu/doc/caw.html\#Canvas.tag_lower "Gérer la superposition d'éléments graphiques dans un Canvas"
[tkinter-canvas-tag_bind]: http://tkinter.fdex.eu/doc/caw.html#Canvas.tag_bind "Associer un tag à un élément graphique du Canvas"
[tkinter-events]: http://tkinter.fdex.eu/doc/event.html#sequence-d-evenements
[tkinter-vars]: http://tkinter.fdex.eu/doc/ctrvar.html
[bounding-box]: https://en.wikipedia.org/wiki/Minimum_bounding_box "Qu'est-ce qu'une bounding box ?"
[tuto-tkinter-developpez.com]: https://tarball69.developpez.com/tutoriels/python/isn-passe-ton-faq/#LIII-C-8 "Un didacticiel sur Tkinter du site developpez.com"
[detection-collision]: https://fr.wikipedia.org/wiki/D%C3%A9tection_de_collision#Jeux_vid%C3%A9o
[stack-oy-bas]: https://gamedev.stackexchange.com/questions/83570/why-is-the-origin-in-computer-graphics-coordinates-at-the-top-left
[analog-tv]: https://en.wikipedia.org/wiki/Analog_television#Displaying_an_image
[ordinateur]: https://fr.wikipedia.org/wiki/Ordinateur
[konrad-zuse]: https://fr.wikipedia.org/wiki/Ordinateur#Ann%C3%A9es_1930
[entrelacement]: https://fr.wikipedia.org/wiki/Entrelacement_(vid%C3%A9o)
[stack-get-widget-size]: http://stackoverflow.com/questions/3950687/how-to-find-out-the-current-widget-size-in-tkinter
[stack-canvas-resize]: http://stackoverflow.com/questions/22835289/how-to-get-tkinter-canvas-to-dynamically-resize-to-window-width
