# À la découverte des nombres complexes

## Le contexte

Ces nouveaux nombres ont été introduits en tant qu'_artifice de calcul_
(une astuce, quoi), permettant de résoudre certains problèmes jusqu'à alors
insolubles. Au XVI^ème^, des mathématiciens italiens finissent par
mettre au point une technique de résolution des équations du 3^ème^
degré de la forme $x^3+px+q=0$ (où $p$ et $q$ sont des nombres qui sont
à préciser en fonction du contexte).

??? info "Remarque"

    La forme générale d'une équation du 3<up>ieme</up> degré est $ax^3+bx²+cx+d=0$,
    mais on peut démontrer qu'il est _toujours_ possible de se ramener à
    la forme « réduite » $x^3+px+q=0$. Les mathématiciens italiens du
    XVI^ème^ siècle l'ont fait, ce qui est un exploit car ils
    n'utilisaient pas nos notations algébriques modernes (contrairement
    à ce que certains d'entre vous pensent peut-être, le _calcul algébrique_
    constitue un _progrès remarquable_, tant en terme de simplicité que d'efficacité !). 


## La formule de CARDAN et deux exemples d'application

!!! ok "Formule de CARDAN"
    
    L'équation $x^3+px+q=0$ a pour solution
    $\alpha = \sqrt[3]{-\dfrac q2 - \dfrac12\sqrt{\dfrac{4p^3+27q^2}{27}}} + \sqrt[3]{-\dfrac q2+\dfrac12\sqrt{\dfrac{4p^3+27q^2}{27}}}$.

??? info "Remarques"

    * Un mathématicien trouverait _a priori_ à redire à la formulation
    précédente, car il faudrait s'assurer que la quantité présente sous
    les racines carrées sont bien _positives_. Or ça n'est pas _toujours_
    le cas... Mais comme c'est _justement_ ce problème que l'on souhaite
    contourner, nous allons oublier ce « détail » :wink: !
    
    * $\sqrt[3]{x}$ désigne la _racine cubique_ de $x$ : c'est le nombre
    $y$ qui vérifie $y^3=x$. Ainsi, $\sqrt[3]{-8} = -2$ car $(-2)^3=-8$.
        
        C'est la même logique que pour la _racine carrée_ : on note $y = \sqrt x$
        le nombre $y$ dont le carré vaut $x$, c'est-à-dire tel que $y^2=x$.
        On pourrait (on _devrait_ !) écrire $y=\sqrt[2]{x}$, mais on a décidé
        de considérer que _par défaut_, le symbole $\sqrt{\rule{0ex}{1ex}\ }$
        désigne la _racine **carrée**_ de $x$.

    * La formule de CARDAN permet de trouver **une** solution, mais il peut
    y en avoir d'**autres**. Notez qu'à l'époque, il n'était pas du tout
    évident qu'un problème se ramenant à un raisonnement mathématique puisse
    posséder _plusieurs_ solutions. Notez également que la formule de CARDAN
    n'est vraie **que** si $4p^3+27q^2\geqslant 0$...
    
    * Girolamo CARDANO, dont le nom a été francisé en Jérôme CARDAN, est
    également célèbre pour avoir inventé... oui ! le [cardan][wikipedia-cardan]{target='__blank' rel='noopener'},
    dispositif mécanique qui permet la transmission d'un mouvement rotatif
    entre deux axes qui forment un angle.
    
        <center>
        ![](https://upload.wikimedia.org/wikipedia/commons/3/31/Universal_joint_transparant.gif)
        </center>
    
    * Ce même Girolamo CARDANO n'a en fait... **pas** trouvé la formule
    de CARDAN ! C'est un certain [Niccolò FONTANA](https://fr.wikipedia.org/wiki/Niccol%C3%B2_Fontana_Tartaglia){target='__blank' rel='noopener'},
    surnommé « Tartaglia », ce qui signifie « le bègue ». En effet, âgé
    de 13 ans, sa mère et lui se réfugient dans le cathédrale de BRESCIA
    pour échapper aux assauts des soldats français de Louis XII. Mais les
    soldats y pénètrent et... je vous laisse deviner ce qu'ils y font. Niccolò
    FONTANA est laissé pour mort, avec une fracture du crâne et un coup
    de sabre à travers la mâchoire et le palais : bouche tailladée, dents
    brisées, maxillaires et palais fracturés... Ces blessures lui laisseront
    un défaut d'élocution qui mettra du temps à disparaître.
    
        * [Plus de détails de sa biographie sur la page Wikipedia qui lui est consacrée](https://fr.wikipedia.org/wiki/Niccol%C3%B2_Fontana_Tartaglia#Biographie){target='__blank' rel='noopener'}.
        
        * Tartaglia a révélé sa formule à CARDANO sous la forme... d'un poème :
        <center>
        [![](images/resolution-3eme-degre-tartaglia.gif)](https://www.maths-et-tiques.fr/index.php/histoire-des-maths/nombres/histoire-de-l-algebre#signet9){target='__blank' rel='noopener'}
        </center>
        **Question :** préférez-vous le poème, ou la formule :wink: ?


### Application 1 : tout fonctionne bien

Résolvons l'équation $x^3+3x-4=0$.
        
=== "Questions"

    1. Vérifier que l'application de la formule de CARDAN donne $\alpha=\sqrt[3]{2-\sqrt5} +\sqrt[3]{2+\sqrt5}$.

    2. Vérifier à l'aide la calculatrice que $\alpha=1$.

    3. Vérifier que $1$ est bien solution de l'équation.

    4. Vérifier à la calculatrice puis à la main que $\left(\dfrac{1+\sqrt5}2\right)^3=2+\sqrt5$ et $\left(\dfrac{1-\sqrt5}2\right)^3=2-\sqrt5$.
        
        _Vous pourrez commencer par calculer $\left(\frac{1+\sqrt5}2\right)^2$ puis multiplier le résultat par $\left(\frac{1+\sqrt5}2\right)$_.

    5. En déduire qu'on a bien $\alpha=\sqrt[3]{2-\sqrt5}+\sqrt[3]{2+\sqrt5}=1$.
    
=== "Indications"

    Les 3 premières questions n'en méritent pas. La 4^ème^ nécessitera d'utiliser
    des identités remarquables et la double distributivité.
    
    * Identités remarquables : $(a+b)^2 = a^2 + 2ab + b^2$ et $(a-b)^2 = a^2 - 2ab + b^2$.
    
    * Double distributivité : $(a+b)(c+d) = ac + ad + bc + bd$.
    
    Il faudra aussi se souvenir de règles sur les fractions.
    
    * Addition de fractions de même dénominateur : $\dfrac ac + \dfrac bc = \dfrac{a+b}c$
    
    * Multiplication de fractions : $\dfrac ab \times \dfrac cd = \dfrac{a\times c}{b\times d}$
    
    Il faudra enfin se souvenir de règles de calcul sur les puissances et les racines carrées.
    
    * $\left(\dfrac ab\right)^n = \dfrac{a^n}{b^n}$.
    
    * $\sqrt{x}^2 = x$ (si $x\geqslant 0$).
    
    * $\sqrt{a\times b} = \sqrt{a} \times \sqrt{b}$ (si $a\geqslant 0$ et $b\geqslant 0$).
    
    _**Rappel :** une règle de calcul peut être utilisée indifféremment pour passer de l'expression de gauche à celle de droite, ou de celle droite à celle de gauche !_
    

=== "Réponses"

    1. Il suffit d'appliquer la formule de CARDAN avec $p=3$ et $q=-4$. On trouve alors $4^3+27q^2 = 4 \times 3^3 + 27 \times (-4)^2 = 4 \times 27 + 27 \times 16 = 27 \times (4+16) = 27 \times 20$ (car $27$ est un facteur commun évident). On a donc $\dfrac{4^3+27q^2}{27} = \dfrac{27 \times 20}{27} = 20$. On en déduit que $\sqrt{\dfrac{4^3+27q^2}{27}}=\sqrt{20}=\sqrt{4 \times 5} = \sqrt{4} \times \sqrt{5} = 2\sqrt5$. Comme on trouve cette expression dans les deux termes qui composent la formule de CARDAN, le calcul s'en trouve simplifié : $\alpha = \sqrt[3]{-\dfrac{(-4)}2 - \dfrac12 \times 2\sqrt5} + \sqrt[3]{-\dfrac{(-4)}2 + \dfrac12 \times 2\sqrt5} = \sqrt[3]{2 + \sqrt5} + \sqrt[3]{2 - \sqrt5}$.
    
    * $\left(\dfrac{1+\sqrt5}2\right)^2 = \left(\dfrac12+\dfrac{\sqrt5}2\right)^2 = \left(\dfrac12\right)^2 + 2\dfrac12\dfrac{\sqrt5}2 + \left(\dfrac{\sqrt5}2\right)^2 = \dfrac14 + \dfrac{\sqrt5}2 + \dfrac54 = \dfrac32 + \dfrac{\sqrt5}2$.
    
    * $\left(\dfrac{1+\sqrt5}2\right)^3 = \left(\dfrac32 + \dfrac{\sqrt5}2\right)\left(\dfrac12+\dfrac{\sqrt5}2\right) = \dfrac32 \times \dfrac12 + \dfrac32 \times \dfrac{\sqrt5}2 + \dfrac{\sqrt5}2 \times \dfrac12 + \dfrac{\sqrt5}2 \times \dfrac{\sqrt5}2 = \dfrac34 + \dfrac42\dfrac{\sqrt5}2 + \dfrac54$. On regroupe $\dfrac34 + \dfrac54 = \dfrac84 = 2$ et on simplifie $\dfrac{4\sqrt5}4=\sqrt5$. D'où $\left(\dfrac{1+\sqrt5}2\right)^3 = 2 + \sqrt5$.
    
    * On montrerait de même (mais avec la 2^ème^ identité remarquable) que $\left(\dfrac{1-\sqrt5}2\right)^3 = 2 - \sqrt5$ (faites-le !).
    
    * La dernière question est alors simple : la racine cubique « défaisant » ce que fait la fonction cube[^fonction_carré], on a donc $\sqrt[3]{2 + \sqrt5} = \sqrt[3]{\left(\dfrac{1 + \sqrt5}2\right)^3} = \dfrac12 + \dfrac{\sqrt5}2$ et, de la même façon (en admettant que vous ayez fait le second calcul ou — bien moins utile pour vous — que vous me fassiez confiance), $\sqrt[3]{2 - \sqrt5} = \sqrt[3]{\left(\dfrac{1 - \sqrt5}2\right)^3} = \dfrac12 - \dfrac{\sqrt5}2$. On trouve donc finalement $\alpha=\sqrt[3]{2 - \sqrt5}+\sqrt[3]{2 + \sqrt5} = \dfrac12 + \dfrac{\sqrt5}2 + \dfrac12 - \dfrac{\sqrt5}2 = \dfrac12 + \dfrac12 = 1$.
    
    **Conclusion :** la formule de CARDAN donne bien _une_ solution de l'équation $x^3+3x-4=0$, au prix de calculs longs et un rien fastidieux, lorsqu'on n'est pas entraîné. Cela relativise _un peu_ l'intérêt de la technique qui, dans des cas « simples », conduit à des efforts conséquents. Mais quand on n'a pas mieux, on n'a pas le choix. Et au XVI^ème^ siècle, il n'y avait aucun _autre_ choix...


### Application 2 : la formule dysfonctionne !

Tentons de résoudre l'équation $x^3-15x-4=0$.

=== "Questions"

    
    1. Tracez la courbe représentant la fonction $f(x)=x^3-15x-4$ pour $x\in[-5\,;5]$
    (avec une calculatrice ou [GeoGebra](https://www.geogebra.org/download){target='__blank' rel='noopener'}.
    Vous réglerez votre affichage avec des ordonnées $y\in[-40\,;40]$.

    2. Manifestement, l'équation $f(x)=0$ admet 3 solutions, dont l'une semble entière : laquelle ? Vérifiez par le calcul que tel est bien le cas.

    3. Vérifiez aussi que la formule de CARDAN  _donnerait_  (en se forçant
    à l'utiliser) la solution $\alpha=\sqrt[3]{2+\sqrt{-121}}+\sqrt[3]{2-\sqrt{-121}}$.

=== "Indications"

    <center>
    ![](images/cardan_fonction2.png){width=600}
    </center>

=== "Réponses"
    
    1. Cf. l'indication qui précède.
    
    2. Il semble clair que $x=4$ est un bon candidat-solution. Et en effet, lorsqu'on remplace $x$ par $4$ dans l'expression $x^3-15x-4$, on obtient $4^3 - 15 \times 4 -4 = 64 - 60 - 4 = 0$. Donc $x=4$ est bien une  solution entière de l'équation $x^3-15x-4 = 0$.
    
    3. Faisons comme si la formule était applicable (en fait, elle ne l'est pas, comme on s'en apercevra en cours de calcul).
    
        * L'équation $x^3-15x-4 = 0$ est bien de la forme $x^3+px+q=0$ avec $p=-15$ et $q=-4$.
        
        * $4p^3+27q^2=4\times(-15)^3+27\times(-4)^2=-13\,068$ (vérifiez à la calculatrice !). On sait donc à cet instant qu'on va avoir un souci avec la racine carrée, car on est censé calculer $\sqrt{\dfrac{4p^3+27q^2}{27}}$ ! Forçons-nous à continuer, l'air de rien...
        
        * $\dfrac{4p^3+27q^2}{27} = -484 = 4 \times (-121) = 2^2 \times 11^2 \times (-1)$.
        
        * Continuons de « faire comme si » : $\sqrt{-484} = \sqrt{2^2} \times \sqrt{11^2} \times \sqrt{-1}  = 2 \times 11 \times \sqrt{-1} = 22 \sqrt{-1}$.
        
        * On en déduit que $\sqrt[3]{-\dfrac q2 - \dfrac12\sqrt{\dfrac{4p^3+27q^2}{27}}} = \sqrt[3]{2-11\sqrt{-1}}$, puis que la solution de l'équation $x^3-15x-4=0$ _pourrait_ s'écrire $\alpha = \sqrt[3]{2-11\sqrt{-1} }+ \sqrt[3]{2+11\sqrt{-1}}$.
    
Cette écriture n'a _aucun sens_ ! Pourtant, il est surprenant qu'**aucune**
solution de l'équation ne soit donnée par la formule, alors qu'il en existe
une simple... **En 1545, CARDAN demande alors au lecteur de son grand œuvre,
[_Ars Magna_](https://fr.wikipedia.org/wiki/Ars\_Magna\_(Girolamo\_CARDANO)){target='__blank' rel='noopener'},
de faire preuve d'_imagination_ en s'autorisant à écrire une telle expression**.
Il y  explique également **comment calculer avec ces nombres**. Il les baptise
_quantités sophistiquées_ ([se reporter à Wikipedia pour plus de détails][wikipedia-complexes]{target='__blank' rel='noopener'}).


### Stratégie de contournement : les nombres imaginaires

En 1572, un autre mathématicien italien, Rafaël BOMBELLI, pose les bases
de la théorie des **nombres imaginaires** et en formalise les règles de calcul :
cela revient à _imaginer_  qu'il existe un _ensemble qui englobe $\mathbb R$_
(l'ensemble des nombres réels), dans lequel il existerait des racines carrées
de nombres négatifs. L'écriture $2+\sqrt{-121}$ devient alors $2+11\sqrt{-1}$,
car on s'autorise à écrire $\sqrt{-121}=\sqrt{121\times(-1)}=\sqrt{121}\times\sqrt{-1}=11\sqrt{-1}$.

??? warning "Attention : problème subtil"

    La notation $\sqrt{-1}$ est problématique : _la fonction racine carrée
    que nous connaissons **ne**  peut **pas** « fonctionner » de façon **cohérente**
    dans ce sur-ensemble de $\mathbb R$ !_ En effet, on aurait alors (en
    appliquant les règles de calcul valables avec les racines carrées « normales ») :
    
    * $\sqrt{-1}\times\sqrt{-1} =\big(\sqrt{-1}\big)^2=-1$ ;
    
    * $\sqrt{-1}\times\sqrt{-1}=\sqrt{-1\times(-1)}=\sqrt{+1}=1$.

    On conclurait alors que $-1=1$... Aïe aïe aïe : on a tout cassé les maths :scream: ?!!??
    Non ! On s'en sort en _imaginant_ alors, dans cet ensemble qui contient
    tout $\mathbb R$, de noter $\mathbf i$ le nombre dont le carré est $-1$.
    
    
!!! ok "Notation & règle de calcul"

    * On définit et note $\mathbf i$ le nombre tel que $\mathbf i^2=-1$.
    On l'appelle **nombre imaginaire**.
    
    * Un **nombre complexe $z$ est un nombre de la forme $a+\mathbf i b$**,
    où $a$ et $b$ désignent des nombres réels (éventuellement nuls). Ici,
    le nombre _réel_ $b$ vient multiplier le nombre imaginaire $\mathbf i$.
        
    * On calcule avec les nombres qui impliquent $\mathbf i$ comme avec les
    autres nombres (ou expressions algébriques), en appliquant les mêmes
    règles. **Mais on remplace $\mathbf i^2$ par $-1$ chaque fois que l'on
    rencontre l'expression $\mathbf i^2$**.
    
    
??? info "Remarque"

    En Sciences Physiques comme en Python, on utilise le symbole $\mathbf j$
    au lieu du symbole $\mathbf i$. En physique, c'est pour éviter la confusion
    avec l'intensité d'un courant électrique, souvent notée $i$, justement.  


**Reprenons l'exemple vu précédemment dans l'application n°2 :** l'écriture $2+11\sqrt{-1}$ devient
$2+11\mathbf i$, et l'on peut achever le calcul :
   
* une calculatrice _adaptée_ connaît et sait calculer avec le nombre $\mathbf i$.
On peut alors constater que l'on a effectivement $\sqrt[3]{2+11\mathbf i} + \sqrt[3]{2-11\mathbf i} = 4$.

    _Il faudra repérer le symbole $\mathbf i$ sur son clavier. Vous pourrez utiliser
l'application Numworks sur votre téléphone, ou le simulateur de la calculatrice
Numworks à l'adresse_ [https://www.numworks.com/fr/simulateur/][numworks]{target='__blank' rel='noopener'}.
        
* en appliquant les règles de calcul _habituelles_, mais en remplaçant $\mathbf i^2$
par $-1$ **partout où $\mathbf i^2$ apparaît**, on peut montrer que $(2+\mathbf i)^3=2+11\mathbf i$
et $(2-\mathbf i)^3 = 2-11\mathbf i$ (vérifiez-le en montrant d'abord que
$(2 + \mathbf i)^2 = 3 + 4\mathbf i$ puis que $(2 + \mathbf i)^3 = (3 + 4\mathbf i)(2+i)$
donne bien le résultat attendu ; procédez de même avec $(2-\mathbf i)^2 = 3 - 4\mathbf i$).  
Comme $\alpha = \sqrt[3]{2+11\mathbf i} + \sqrt[3]{2-11\mathbf i}$, on peut
alors écrire $\alpha = \sqrt[3]{\big(2+\mathbf i\big)^3} + \sqrt[3]{\big(2-\mathbf i\big)^3}$
et, comme la racine cubique « défait » les cubes, on obtient finalement
$\alpha = (2+\mathbf i) + (2-\mathbf i) = 4$.


## Définition des nombres complexes

Il faudra attendre près de 300 ans, et le concours de mathématiciens célèbres
tels qu'EULER, GAUSS, HAMILTON et CAUCHY pour donner un sens mathématiquement
correct aux « contorsions » imaginées par BOMBELLI pour faire fonctionner la
formule de CARDAN même quand elle ne le devrait pas !

!!! ok "Définitions & propriétés"

    * On définit et note $\mathbf i$ le nombre tel que $\mathbf i^2=-1$.
    On l'appelle **nombre imaginaire**.
    
    * Un **nombre complexe $z$ est un nombre de la forme $a+\mathbf i b$**,
    où $a$ et $b$ désignent des nombres réels (éventuellement nuls). $z$
    comporte donc deux parties, l'une dite réelle (c'est $a$), l'autre
    dite imaginaire (c'est $b$), dans laquelle le nombre _réel_ $b$ vient
    multiplier le nombre imaginaire $\mathbf i$.
    
    * La forùe $a+ \mathbf i b$ est appelée **forme algébrique** du nombre
    complexe (il existe une autre forme, que l'on verra plus loin).
    
    * Pour un nombre complexe $z=a+b\mathbf i$, la **partie réelle $a$**
    se note aussi $\Re(z) = \mathrm{Re}(z) = a$ ; la **partie imaginaire $b$**
    s'écrit $\Im(z) = \mathrm{Im}(z) = b$.
    
    * On calcule avec les nombres qui impliquent $\mathbf i$ comme avec les
    autres nombres (ou expressions algébriques), en appliquant les mêmes
    règles. **Mais on remplace $\mathbf i^2$ par $-1$ chaque fois que l'on
    rencontre cette expression**.
    
    * On représente graphiquement un nombre complexe au moyen d'un point
    $M$ de coordonnées $(a \, ; b)$. Dans ce contexte, l'axe des abscisses est
    appelé _l'axe des réels_ , celui des ordonnées est rebaptisé _axe des
    imaginaires_.  
    **Vocabulaire additionnel :**
        * le point $M$ a pour **affixe** le nombre complexe $z$ ;
        * le nombre complexe $z$ a pour **image** le point $M$.
    
    * Lorsque $z = a + \mathbf i b$ est un nombre complexe, le **nombre complexe
    conjugué**, noté $\bar z$ et défini par $\bar z = a - \mathbf i b$, est
    tel que $z \times \bar z = a^2 + b^2 \in \mathbb R$.  
    Le point $N$ d'affixe $\bar z$ a la même abscisse que $M$ mais une ordonnée
    _opposée_ : $M$ et $N$ sont donc _symétriques par rapport à l'axe des
    réels_ (l'axe des abscisses).
    
    * **Exemple :** le nombre complexe $z=3 + 2\mathbf i$ a pour image $M(3\,;2)$.
    Le point $N(3\,;-2)$ a pour affixe le nombre complexe $\bar z = 3 - 2\mathbf i$.
    $M$ et $N$ sont symétriques par rapport à l'axe des réels. Enfin,
    $z\times\bar z=(3+2\mathbf i)(3-2\mathbf i)=3^2-(2\mathbf i)^2$ d'après
    la 3^ème^ identité remarquable et $(2\mathbf i)^2=(2\mathbf i)\times(2\mathbf i)=4\mathbf i^2=-4$,
    donc $z\times\bar z=9-(-4)=9+4=13$.
    
        <div style="text-align: center">![](images/représentation-géométrique-complexe-1.png){width=300}</div>

=== "Exemples de calculs simples"

    Soient les nombres complexes $z_1 = 3 + 2\mathbf i$, $z_2 = 3 - 2 \mathbf i$,
    $z_3 = -\sqrt3 + 3\mathbf i$ et $z_4 = 3 - \mathbf i \sqrt3$. Calculer
    et donner le résultat sous forme algébrique de :
    
    * $z_1+z_2$  
    * $z_1-z_2$
    * $z_1\times z_2$
    * $\dfrac{z_1}{z_2}$
    * $z_1^2 = (z_1)^2$
    * $z_3^3 = (z_3)^3$
    * $z_4^3 = (z_4)^3$

=== "Indications"

    * Pour le calcul $z_1\times z_2$, on utilise la _double distributivité_ :
    $(a+b) \times (c+d) = ac + ad + bc + bd$.
    * Pour obtenir la forme algébrique de $\dfrac{z_1}{z_2}$, on a un souci
    avec la présence d'un $\mathbf i$ au dénominateur. Pour s'en débarrasser,
    vous multiplierez $\dfrac{z_1}{z_2}$ par $\overline{z_2}$ au _numérateur_
    **et** au _dénominateur_, ce qui ne changera pas la valeur de la fraction,
    mais transformera le dénominateur en un **nombre réel** (cf. propriété
    de la multiplication par le nombre complexe conjugué).
    * Pour les élévations au cube, vous procéderez en deux temps, comme
    au début de ce document : $(z_3)^3 = (z_3)^2 \times z_3$. Naturellement,
    il faudra appliquer une identité remarquable pour obtenir la forme algébrique
    de $(z_3)^2$.

=== "Réponses"

    N'importe quelle calculatrice adaptée au lycée général et technologique
    vous permettra de contrôler vos résultats. _Il faudra juste repérer
    le symbole $\mathbf i$ sur son clavier !_ En l'absence de telle calculatrice,
    vous pourrez utiliser l'application Numworks sur votre téléphone, ou
    le simulateur de la calculatrice Numworks à l'adresse [https://www.numworks.com/fr/simulateur/][numworks]{target='__blank' rel='noopener'}.  
    Enfin, vous pourrez également utiliser [GeoGebra][geogebra-download]{target='__blank' rel='noopener'},
    sur votre ordinateur, votre téléphone ou [directement dans n'importe quel navigateur][geogebra-calculator]{target='__blank' rel='noopener'}.
    Vous pourrez vous aider de l'illustration ci-dessous : remarquez qu'il
    faut choisir le mode « formel ».  
    _**Attention,** la lettre « i » ne correspond pas, dans GeoGebra, au $\mathbf i$ spécifique
    qui vérifie $\mathbf i^2=-1$ ! Pour obtenir « le bon i », il faut presser simultanément
    les touches_ ++alt+i++, ce qui affiche dans un premier temps le caractère
    « í », qui est ensuite interprêté convenablement par le logiciel.
    
    <div style="text-align: center">![](images/geogebra_calculs_complexes.gif){width=1024}</div>

??? info "Remarque"

    Il arrive parfois qu'un calcul avec des nombres complexes donne un résultat
    _réel **pur**_ (un nombre complexe dont la partie _imaginaire_ est nulle).
    
    De même, il peut arriver qu'un calcul donne un nombre complexe dont
    la partie réelle est nulle : on parle alors de nombre _imaginaire **pur**_.


## Utilité des nombres complexes
 
Pourquoi s'intéresser aux nombres complexes ? Parce que bien des années après leur introduction, on s'apercevra de la puissance qui se cache derrière eux.

* On les retrouve en Sciences Physiques, où ils permettent de simplifier
grandement certains calculs et aident à modéliser de nombreux phénomènes :

    * ils sont spécialement utiles pour la modélisation des phénomènes qui
    se déroulent dans les circuits électroniques ;
    * ils apparaissent dès qu'on s'intéresse aux courants alternatifs, aux
    ondes et aux champs électro-magnétiques (on les rencontre donc en optique !),
    en mécanique quantique...
    
* Ce sont des objets **fondamentaux** en mathématiques et indispensables
en informatique :

    * ils ont donné naissance à des [objets étranges mais fort utiles : les _fractales_](https://www.youtube.com/watch?v=h3rshqpr3JY){target='__blank' rel='noopener'} (qui, accessoirement, donnent également de très jolies figures). Et les applications
    concrètes ne sont jamais loin : sachez par exemple que dans le marché de l'art,
    [les fractales servent à différencier les faux tableaux des originaux](https://www.youtube.com/watch?v=51dFGX5g4So){target='__blank' rel='noopener'} ;
    * on les rencontre en analyse de FOURIER, un domaine fondamental, sans
    lequel la [numérisation du monde sensible](https://www.youtube.com/watch?v=r6sGWTCMz2k){target='__blank' rel='noopener'} et toutes les applications informatiques
    qui en découlent auraient été _impossibles_ ! 
    * en informatique, ils facilitent grandement l'animation d'objets graphiques !
    Vous vous en apercevrez d'ailleurs sous peu, le document suivant visant
    justement à vous faire réaliser un petit programme en Python permettant
    quelques manipulations graphiques élémentaires (translations, rotations
    et dilatations) à l'aide de nombres complexes.


## Forme trigonométrique des nombres complexes

On a vu qu'un complexe $z$ pouvait repérer un point $M$. Dans ces conditions,
il repère aussi le vecteur $\overrightarrow{OM}$. Une _autre_ façon de repérer
le point $M$ ou le vecteur $\overrightarrow{OM}$ serait de connaître la
longueur $OM$ et l'angle $\theta$  entre les vecteurs $\vec u$ et
$\overrightarrow{OM}$ — comme sur un vieux radar, en somme.

<center>

| <center>Une autre façon de repérer un point (ou un vecteur)</center>              | <center>Un radard « à l'ancienne »</center> |
| --------------------------------------------------------------------------------- | ------------------------------------------- |
| <center>![](images/représentation-géométrique-complexe-2.png){width=400}</center> | <center>![](images/radar.gif)</center>      |
| _**Remarque :** les mesures des angles sont comptées positivement lorsqu'elles sont effectuées selon le sens trigonométrique (c'est le sens anti-horaire — celui des ronds-points !)_ | _**Remarque :** ici, le faisceau du radar **ne** tourne **pas** dans le sens trigonométrique (et c'est regrettable), mais l'analogie reste pertinente..._ |

</center>

!!! info "Définition"

    Pour un complexe $z$ d'image le point $M$, on appelle **module  de $z$ et on note $|z|$ la distance $OM$**. L'angle $\theta = \big( \vec u \, ; \overrightarrow{OM} \big)$ est appelé l'**argument de $z$, et noté $\arg z$**. La donnée de $|z|$ et de $\theta$ constitue la **forme trigonométrique** de $z$. On note $\big[ |z| \, ; \theta \big]$ ou $\big[ \rho \, ; \theta \big]$ (on écrit souvent $\rho$ à la place de $|z|$).

??? note "Remarques"

    * $\theta$ est une lettre grecque qui s'appelle « theta », et qui **ne**
    correspond à **aucune** lettre de l'alphabet latin.
    * $\rho$ est une lettre grecque qui se prononce « rhô », et qui correspond
    à notre « r ».
    * Cette notation peut prêter à confusion avec la notation des intervalles,
    découverte en classe de seconde. En terminale, certains d'entre vous
    verront une nouvelle notation, dite « notation exponentielle » :
    $\big[ \rho \, ; \theta \big] = \rho \mathrm{e}^{\mathbf i \theta}$.
    Mais encore faut-il que vous découvriez la notion d'exponentielle...

On peut démontrer les propriétés suivantes (on _ne_ le fera _pas_ ici) :

!!! warning "Propriétés"

    Soient 2 nombres complexes $z=\big[\rho\,; \theta\big]$ et $z'=\big[\rho'; \theta'\big]$. Alors :
    $z\times z' = \big[ \rho \times \rho' \, ; \theta + \theta' \big]$ et
    $\dfrac z{z'}  = \left[ \dfrac{\rho}{\rho'} \, ; \theta-\theta' \right]$.

    Autrement dit :
    
    * lorsqu'on multiplie deux nombres complexes, leurs modules se multiplient et leurs arguments s'additionnent ;    
    * lorsqu'on divise deux nombres complexes, leurs modules se divisent et leurs arguments se soustraient (il faut _bien faire attention à l'ordre !_).


## Nombres complexes et transformations graphiques

### Translations

On a vu qu'un nombre complexe $z$ pouvait être associé à un point $M$ du
plan, donc au vecteur $\overrightarrow{OM}$ et, par suite, à la translation
associée.

Ainsi, lorsqu'on enchaîne deux translations, on ajoute deux vecteurs, ce
qui revient à ajouter deux nombres complexes.


### Rotations et dilatations

D'après les propriétés vues à la [section précédente sur la forme trigonométrique](#forme-trigonometrique-des-nombres-complexes),
multiplier un nombre complexe $z$, qui correspond à un point $M$ et donc
au vecteur $\overrightarrow{OM}$, par un autre nombre complexe $m$, amène
à créer un nouveau point $N$ qui vérifie $ON = OM \times |m|$ et tel que
l'angle orienté $(\overrightarrow{OM} \, ; \overrightarrow{ON}) = \theta$
(on compte positivement un angle lorsqu'on tourne dans le sens anti-horaire
ou _sens trigonométrique_ ; négativement lorsqu'on tourne dans le sens horaire).

Ainsi, le vecteur $\overrightarrow{ON}$ s'obtient en « faisant tourner »
le vecteur $\overrightarrow{OM}$ de l'angle $\arg(m)$ et en « dilatant »
sa norme d'un facteur $|m|$.


### Application aux interfaces graphiques

Sur vos appareils informatiques (ordinateurs, téléphones ou tablettes),
il arrive fréquemment que des éléments graphiques subissent des transformations :
une icône qu'on sélectionne puis qu'on déplace, une image ou une fenêtre
qu'on agrandit, ou à laquelle on fait subir une rotation...

Pour simplifier, imaginez un simple polygone : une figure composée de plusieurs
point (au moins 3, appelés sommets), reliés par des segments de droites.

* **Pour le déplacer,** il suffit d'ajouter un même nombre complexe aux affixes
de chaque sommet _(rappel : l'affixe d'un point est le nombre complexe associé ;
ainsi, le point de coordonnées $(3 \, ; -2)$ a pour affixe $3 -2 \mathbf i$)_.

* **Pour faire subir une rotation de centre $O$** (origine du repère) à
ce polygone, il suffit de multiplier les affixes de chacun de ses sommets
par un nombre complexe de module 1 _(rappels : le module d'un nombre complexe
correspond à la distance qui sépare le point correspondant de l'origine
$O$ du repère, et lorsqu'on multiplie deux complexes, leurs modules se multiplient)_.

* Lorsqu'on multiplie les affixes des sommets par un même complexe de module
**strictement supérieur à $1$**, la figure subit une **dilatation** (un
agrandissement).

* Lorsqu'on multiplie les affixes des sommets par un même complexe de module
**strictement inférieur à $1$**, la figure subit une **contraction** (un
rétrécissement).

**Grâce aux nombres complexes, on arrive donc à déplacer une figure géométrique
dans toutes les directions, à le redimensionner et à le « faire tourner ».
On dispose donc d'un formalisme puissant pour créer des animations !**

C'est ce qu'on va maintemant chercher à programmer dans le
[document suivant](60-complexes-et-animations.md){target='__blank' rel='noopener'}.


 [^fonction_carré]:
    On n'a pas avec le couple fonction cube $x^3$ / fonction racine cubique $\sqrt[3]x$ les mêmes soucis qu'avec le couple fonction carré $x^2$ / fonction racine carrée $\sqrt x$. En effet, la racine carrée « défait _mal_ » ce que fait le carré, car la fonction carré n'est pas monotone sur $\mathbb R$ (elle change de variation : d'abord décroissante sur $]-\infty\,;0]$ puis croissante $[0\,;+\infty[$). Ainsi, $9$ est aussi bien le carré de $3$ que de $-3$, mais une fonction ne peut, pour un nombre donné, avoir qu'**une seule image** associée à ce nombre. Ainsi, on doit choisir comment $9$ est transformé par la fonction $\sqrt{\vphantom{t}\ }$ : on a décidé que $\sqrt9$ donnerait $3$, laissant de côté la valeur $-3$ (qu'on obtient avec $-\sqrt9$). La fonction cube, elle, est strictement croissante sur $\mathbb R$ : on n'a alors aucun souci, une image $y$ obtenue par la fonction cube ne pouvant correspondre qu'à un et un seul antécédent $x$, qui est tel que $x^3=y$. Ainsi, la fonction racine cubique $\sqrt[3]{x}$ compense-t-elle parfaitement l'action de la fonction cube $x^3$...

[wikipedia-complexes]: https://fr.wikipedia.org/wiki/Histoire_des_nombres_complexes#Les_nombres_complexes_comme_objet_alg%C3%A9brique 
[wikipedia-cardan]: https://fr.wikipedia.org/wiki/Joint_de_Cardan
[numworks]: https://www.numworks.com/fr/simulateur/
[geogebra-download]: https://www.geogebra.org/download
[geogebra-calculator]: https://www.geogebra.org/calculator
