# Sommaire de l'espace « mathématiques & Python »

* [Tracer des fonctions en Tkinter](20-tracer-fonction/){target='__blank' rel='noopener'}.
Il est certain qu'utiliser [Maplotlib](https://matplotlib.org/){target='__blank' rel='noopener'} serait bien plus simple (et offrirait bien plus de facilités). Mais c'est bien aussi, d'y aller en « mode _roots_ ».

* [Découverte des nombres complexes](50-complexes){target='__blank' rel='noopener'}. Une introduction aux nombres complexes fidèle à l'Histoire.

* [Application informatique des nombres complexes : réaliser des animations graphiques](60-complexes-et-animations){target='__blank' rel='noopener'}. Où l'on voit comment les nombres complexes permettent de facilement déplacer, redimensionner et faire tourner des figures géométriques, ce qui ouvre la voie à la création de petits jeux vidéos.
